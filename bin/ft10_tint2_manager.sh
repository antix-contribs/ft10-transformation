#!/bin/bash
# Tint2 toolbar quick settings, by PPC
#-------------------------------------------------
# Modified by marcelocripe 12/02/2021 - script prepared for translation

        TEXTDOMAINDIR=/usr/share/locale
        TEXTDOMAIN=ft10_tint2_manager

#Variable names, for easier translation:
window_title=$"Toolbar manager"
quick_launch_icons=$"Add/Remove Icons"
position=$"Toolbar Position"
auto_hide=$"Auto-hide toolbar"
resize=$"Toolbar size"
clock=$"12/24h Clock"
show_titles=$"Show window titles in toolbar"
dark_mode=$"Toogle Dark Mode on-off"
cpu_ram=$"Show CPU, RAM"
[ ! -f ~/.config/tint2/tint2rc ] && { 
	yad --title="$window_title" --text="~/.config/tint2/tint2rc not found\nRun tint2 at least once" --button='Ok' 
	exit 1
}

while yad --title="$window_title" --center --form --width=250 \
--field="$quick_launch_icons":BTN "/usr/local/lib/ft10/tim-tint.sh" --field="$position":BTN "/usr/local/lib/ft10/tint2-top-bottom.sh" --field="$auto_hide":BTN "/usr/local/lib/ft10/tint2-toggle-autohide.sh" --field="$resize":BTN "/usr/local/lib/ft10/tint2_outros.sh" --field="$clock":BTN "/usr/local/lib/ft10/tint2-toogle-12-24h.sh" --field="$show_titles":BTN "/usr/local/lib/ft10/tint2-toggle-window-titles.sh" --field="$dark_mode":BTN "/usr/local/lib/ft10/ft10_tint2_toggle-darkmode.sh" --field="$cpu_ram":BTN "/usr/local/lib/ft10/ft10_tint2_toggle-resources.sh" --no-buttons
do
	:
done
