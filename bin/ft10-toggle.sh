#!/bin/bash
#if tint2 config file exists, back it up first
file=~/.config/tint2/tint2rc
if test -f "$file"; then
sed -n '/launcher_item_app/p' ~/.config/tint2/tint2rc > ~/.config/tint2/backed_up_launchers.txt ; cp ~/.config/jgmenu/append.csv ~/.config/jgmenu/appendBAK;
fi
#create ft10 configurations (initial setup)
 ft10-create ;

#If back up files for pinned apps exit, restore them  
file=~/.config/tint2/backed_up_launchers.txt
if test -f "$file"; then
sed -i '/launcher_item_app/d' ~/.config/tint2/tint2rc ; cp ~/.config/jgmenu/appendBAK ~/.config/jgmenu/append.csv ; cat ~/.config/tint2/backed_up_launchers.txt >> ~/.config/tint2/tint2rc 
fi

#Kill any running instance of Tint2, run ft10-start
pkill tint2; ft10-start
