#!/bin/bash
# The script was created by PPC
### Script to show news in a yad window:

site=$(cat ~/.config/antix-newstandrc)
if [ -n "$site" ]; then
    echo "not empty"
else
    echo "empty"
    site=getnews.tech
fi

curl $site | sed -e '1,2d' | sed 's/getnews.tech/http:\/\/getnews.tech/g' > /tmp/news.txt
#Remove number tags
sed -r "s/\x1b\[([0-9]{1,2}(;[0-9]{1,2})*)?[m|K]//g" < /tmp/news.txt > /tmp/news2.txt
#Remove first and last characters from each line
sed -i 's/.$//; s/^.//' /tmp/news2.txt
#Remove first line
sed -e '1,1d' < /tmp/news2.txt > /tmp/news.txt
#display text, with clickable links
yad  --title="News"   --window-icon=gtk-dnd-multiple --center --width=600  --height=550 --button="":1 --text-info <  /tmp/news.txt --show-uri --fontname="ubuntu 12"

#Listen for user selection:
foo=$?

#User clicked the "Configure" button
if [[ $foo -eq 1 ]]; then
 local=$(echo "getnews.tech,ae.getnews.tech,ar.getnews.tech,at.getnews.tech, au.getnews.tech, be.getnews.tech, bg.getnews.tech, br.getnews.tech, ca.getnews.tech, ch.getnews.tech, cn.getnews.tech, co.getnews.tech, cu.getnews.tech, cz.getnews.tech, de.getnews.tech, eg.getnews.tech, fr.getnews.tech, gb.getnews.tech, gr.getnews.tech, hk.getnews.tech, hu.getnews.tech, id.getnews.tech, ie.getnews.tech, il.getnews.tech, in.getnews.tech, it.getnews.tech, jp.getnews.tech, kr.getnews.tech, lt.getnews.tech, lv.getnews.tech, ma.getnews.tech, mx.getnews.tech, my.getnews.tech, ng.getnews.tech, nl.getnews.tech, no.getnews.tech, nz.getnews.tech, ph.getnews.tech, pl.getnews.tech, pt.getnews.tech, ro.getnews.tech, rs.getnews.tech, ru.getnews.tech, sa.getnews.tech, se.getnews.tech, sg.getnews.tech, si.getnews.tech, sk.getnews.tech, th.getnews.tech, tr.getnews.tech, tw.getnews.tech, ua.getnews.tech, us.getnews.tech, ve.getnews.tech, za.getnews.tech")
selection=$(yad --center  --window-icon=gtk-dnd-multiple --title=$"News" --text=" " \
--image="gtk-dnd-multiple" \
--form --date-format="%-d %B %Y" --separator="," --item-separator="," \
--field=$"Site:":CBE \
"$local")
choice=${selection::-1}
touch ~/.config/antix-newstandrc
echo $choice > ~/.config/antix-newstandrc 

#Get script name and path
script_name1=`basename $0`
script_path1=$(dirname $(readlink -f $0))
script_path_with_name="$script_path1/$script_name1"
#restart current script
eval "$script_path_with_name"

fi
