#!/bin/bash
#GUI Script to  search for ip's of shared folders and then connect to the selected on, using zzzfm (may be adapted to any file manager)- By PPC, 7/3/2022, full GPL license
#Dependencies: arp-scan,  smbclient, ip
#Nice create network shares how to: https://www.jortechnologies.com/creating-a-shared-folder-on-linux-and-accessing-to-it-from-windows/
#sudo service smbd restart ### makes sure that Samba is running on the server


echo "" > /tmp/shares.txt
#get elevated priveleges:
gksu shares
#get ips on network (hopefully all shares)
sudo arp-scan -l -x --ignoredups | gawk '/([a-f0-9]{2}:){5}[a-f0-9]{2}/ {print $1}' > /tmp/shares.txt
## also add ip to list (handy is running this on the server, to it's ip is also listed): 
ip -o addr | awk '!/^[0-9]*: ?lo|link\/ether/ {gsub("/", " "); print $2" "$4}'  |  grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}'  >> /tmp/shares.txt
cat /tmp/shares.txt

#Count lines in /tmp/shares.txt, if 0 exit:
lines_with_ips=$(wc -l < /tmp/shares.txt)
BaseLine=1         
 if [ $lines_with_ips < $BaseLine ]
   then
   yad --center --text="No shared network folders found" --button=" x " && exit
 fi

ip=$(yad --no-buttons --title="IPS" --width=550 --height=550 --center --separator=" " --list  --column=""  < /tmp/shares.txt)
sleep

 if [[ $ip = "" ]]; then exit
 fi

smbclient -L $ip -N  | sed '/Printer Drivers/d' | grep "Disk" | cut -d' ' -f1 > /tmp/share_ids.txt

#Count lines in /tmp/share_ids.txt, if it only has one id=$(cat /tmp/share_ids.txt) if it has more than one then:
lines_with_ids=$(wc -l /tmp/share_ids.txt | cut -d' ' -f1)

  if [[ $lines_with_ids = 1 ]]; then
  id=$(cat /tmp/share_ids.txt)
  
   else 
  id=$(yad --no-buttons --title="Shares" --width=550 --height=550 --center --separator=" " --list  --column=""  < /tmp/share_ids.txt)
 fi

sleep 
share=$(echo //$ip/$id/| sed 's/ //g')
zzzfm $share & sleep 1
