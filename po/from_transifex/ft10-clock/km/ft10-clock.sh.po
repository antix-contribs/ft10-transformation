# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:37+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Khmer (https://www.transifex.com/antix-linux-community-contributions/teams/120110/km/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: km\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ft10-clock.sh:5
msgid "FT10-clock"
msgstr "FT10-នាឡិកា"

#: ft10-clock.sh:6
msgid "FT10 Stopwatch"
msgstr "FT10 នាឡិកាបញ្ឈប់"

#: ft10-clock.sh:7
msgid "Stopwatch"
msgstr "នាឡិកាបញ្ឈប់"

#: ft10-clock.sh:8
msgid "Timers"
msgstr "កម្មវិធីកំណត់ម៉ោង"

#: ft10-clock.sh:9
msgid "Alarm"
msgstr "សំឡេងរោទិ៍"

#: ft10-clock.sh:10
msgid "Configure World Clock"
msgstr "កំណត់រចនាសម្ព័ន្ធនាឡិកាពិភពលោក"

#: ft10-clock.sh:11
msgid "Local Time"
msgstr "ម៉ោងu200bក្នុងស្រុក"

#: ft10-clock.sh:12
msgid "Time_Zone"
msgstr "ពេលវេលា_តំបន់"
