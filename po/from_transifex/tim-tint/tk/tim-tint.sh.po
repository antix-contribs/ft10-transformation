# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:38+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Turkmen (https://www.transifex.com/antix-linux-community-contributions/teams/120110/tk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tk\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: tim-tint.sh:60
msgid "Tint2 icons"
msgstr "Tint2 nyşanlary"

#: tim-tint.sh:60
msgid "Double click any Application to remove its icon:"
msgstr "Nyşanasyny aýyrmak üçin islendik Programmany iki gezek basyň:"

#: tim-tint.sh:118
msgid "Double click any Application to move its icon:"
msgstr "Nyşanjyny geçirmek üçin islendik Programmany iki gezek basyň:"

#: tim-tint.sh:226
msgid "ADD ICON!add:FBTN"
msgstr "ICON goşuň!add:FBTN"

#: tim-tint.sh:226
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr "ICON süýşüriň!gtk-go-back-rtl:FBTN"

#: tim-tint.sh:226
msgid "REMOVE ICON!remove:FBTN"
msgstr "ICONY aýyryň!remove:FBTN"
