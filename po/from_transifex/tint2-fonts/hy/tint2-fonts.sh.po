# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:39+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Armenian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hy/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hy\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: tint2_fonts.sh:11
msgid "Toolbar and Menu's Font"
msgstr "Գործիքադարակի և մենյուի տառատեսակը"

#: tint2_fonts.sh:26
msgid "CPU and RAM indicator's Font"
msgstr "CPU և RAM ցուցիչի տառատեսակը"

#: tint2_fonts.sh:41
msgid "Clock's Font"
msgstr "Ժամացույցի տառատեսակը"

#: tint2_fonts.sh:56
msgid "Date's Font"
msgstr "Ամսաթվի տառատեսակը"

#: tint2_fonts.sh:69
msgid "Configure FT10 fonts"
msgstr "Կարգավորեք FT10 տառատեսակները"

#: tint2_fonts.sh:69
msgid "Menu and toolbar:btn"
msgstr "Մենյու և գործիքագոտի:btn"

#: tint2_fonts.sh:69
msgid "CPU and RAM usage indicator:btn"
msgstr "CPU-ի և RAM-ի օգտագործման ցուցիչ:btn"

#: tint2_fonts.sh:69
msgid "Clock:btn"
msgstr "Ժամացույց:btn"

#: tint2_fonts.sh:69
msgid "Date in clock:btn"
msgstr "Ամսաթիվը ժամացույցով:btn"
