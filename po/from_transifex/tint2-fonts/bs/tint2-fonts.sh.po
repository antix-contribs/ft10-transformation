# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:39+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Bosnian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bs\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: tint2_fonts.sh:11
msgid "Toolbar and Menu's Font"
msgstr "Traka sa alatkama i Font menija"

#: tint2_fonts.sh:26
msgid "CPU and RAM indicator's Font"
msgstr "Font indikatora CPU i RAM-a"

#: tint2_fonts.sh:41
msgid "Clock's Font"
msgstr "Font sata"

#: tint2_fonts.sh:56
msgid "Date's Font"
msgstr "Font datuma"

#: tint2_fonts.sh:69
msgid "Configure FT10 fonts"
msgstr "Konfigurišite FT10 fontove"

#: tint2_fonts.sh:69
msgid "Menu and toolbar:btn"
msgstr "Meni i traka sa alatkama:btn"

#: tint2_fonts.sh:69
msgid "CPU and RAM usage indicator:btn"
msgstr "Indikator upotrebe CPU-a i RAM-a:btn"

#: tint2_fonts.sh:69
msgid "Clock:btn"
msgstr "Sat:btn"

#: tint2_fonts.sh:69
msgid "Date in clock:btn"
msgstr "Datum u satu:btn"
