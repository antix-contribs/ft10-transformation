# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:38+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Kannada (https://www.transifex.com/antix-linux-community-contributions/teams/120110/kn/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: kn\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: FT10-yalarm2.sh:79 FT10-yalarm2.sh:87 FT10-yalarm2.sh:132
msgid "Alarm set to"
msgstr "ಅಲಾರಾಂ ಹೊಂದಿಸಲಾಗಿದೆ"

#: FT10-yalarm2.sh:80 FT10-yalarm2.sh:86
msgid "Alarm"
msgstr "ಅಲಾರಂ"

#: FT10-yalarm2.sh:81
msgid "    \\t<b>$alarm_set_to_text $hr:$mn</b> "
msgstr "    \\t<b>$alarm_set_to_text $hr:$mn</b> "

#: FT10-yalarm2.sh:88
msgid "To cancel, right-click icon for menu"
msgstr "ರದ್ದುಗೊಳಿಸಲು, ಮೆನುವಿಗಾಗಿ ಐಕಾನ್ ಮೇಲೆ ಬಲ ಕ್ಲಿಕ್ ಮಾಡಿ"

#: FT10-yalarm2.sh:89
msgid "    \\t<b>$alarm_text $hr:$mn...</b> \\n "
msgstr "    \\t<b>$alarm_text $hr:$mn...</b> \\n "

#: FT10-yalarm2.sh:133
msgid "To cancel, right-click for menu"
msgstr "ರದ್ದುಗೊಳಿಸಲು, ಮೆನುವಿಗಾಗಿ ಬಲ ಕ್ಲಿಕ್ ಮಾಡಿ"

#: FT10-yalarm2.sh:134
msgid "Cancel Alarm"
msgstr "ಅಲಾರಂ ರದ್ದುಮಾಡಿ"

#: FT10-yalarm2.sh:141
msgid ""
"$alarm_set_to_text $hr:$mn\n"
"$cancel_text"
msgstr ""
"$alarm_set_to_text $hr:$mn\n"
"$cancel_text"

#: FT10-yalarm2.sh:145
msgid ""
"$cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel"
msgstr ""
"$cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel"

#: FT10-yalarm2.sh:155
msgid "Are you sure???"
msgstr "ನೀವು ಖಚಿತವಾಗಿರುವಿರಾ???"

#: FT10-yalarm2.sh:156
msgid "  Are you sure to set the alarm for that time tomorrow?"
msgstr "  ನಾಳೆ ಆ ಸಮಯಕ್ಕೆ ಅಲಾರಾಂ ಹೊಂದಿಸುವುದು ಖಚಿತವೇ?"

#: FT10-yalarm2.sh:157
msgid "$confirm_title"
msgstr "$confirm_title"

#: FT10-yalarm2.sh:157
msgid "$are_you_sure_text"
msgstr "$are_you_sure_text"

#: FT10-yalarm2.sh:273
msgid "Duration"
msgstr "ಅವಧಿ"

#: FT10-yalarm2.sh:274
msgid "Interval"
msgstr "ಮಧ್ಯಂತರ"

#: FT10-yalarm2.sh:275
msgid "Repeat"
msgstr "ಪುನರಾವರ್ತಿಸಿ"

#: FT10-yalarm2.sh:276
msgid "seconds"
msgstr "ಸೆಕೆಂಡುಗಳು"

#: FT10-yalarm2.sh:277
msgid "infinite"
msgstr "ಅನಂತ"

#: FT10-yalarm2.sh:280
msgid "Hour::CB"
msgstr "ಗಂಟೆ::CB"

#: FT10-yalarm2.sh:281
msgid "Minutes::CB"
msgstr "ನಿಮಿಷಗಳು::CB"
