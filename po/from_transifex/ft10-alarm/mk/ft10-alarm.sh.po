# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:38+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Macedonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: mk\n"
"Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;\n"

#: FT10-yalarm2.sh:79 FT10-yalarm2.sh:87 FT10-yalarm2.sh:132
msgid "Alarm set to"
msgstr "Алармот е поставен на"

#: FT10-yalarm2.sh:80 FT10-yalarm2.sh:86
msgid "Alarm"
msgstr "Аларм"

#: FT10-yalarm2.sh:81
msgid "    \\t<b>$alarm_set_to_text $hr:$mn</b> "
msgstr "    \\t<b>$alarm_set_to_text $hr:$mn</b> "

#: FT10-yalarm2.sh:88
msgid "To cancel, right-click icon for menu"
msgstr "За да откажете, кликнете со десното копче на иконата за менито"

#: FT10-yalarm2.sh:89
msgid "    \\t<b>$alarm_text $hr:$mn...</b> \\n "
msgstr "    \\t<b>$alarm_text $hr:$mn...</b> \\n "

#: FT10-yalarm2.sh:133
msgid "To cancel, right-click for menu"
msgstr "За да откажете, кликнете со десното копче за менито"

#: FT10-yalarm2.sh:134
msgid "Cancel Alarm"
msgstr "Откажи аларм"

#: FT10-yalarm2.sh:141
msgid ""
"$alarm_set_to_text $hr:$mn\n"
"$cancel_text"
msgstr ""
"$alarm_set_to_text $hr:$mn\n"
"$cancel_text"

#: FT10-yalarm2.sh:145
msgid ""
"$cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel"
msgstr ""
"$cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel"

#: FT10-yalarm2.sh:155
msgid "Are you sure???"
msgstr "Дали си сигурен???"

#: FT10-yalarm2.sh:156
msgid "  Are you sure to set the alarm for that time tomorrow?"
msgstr "  Дали сигурно ќе го поставите алармот за тоа време утре?"

#: FT10-yalarm2.sh:157
msgid "$confirm_title"
msgstr "$confirm_title"

#: FT10-yalarm2.sh:157
msgid "$are_you_sure_text"
msgstr "$are_you_sure_text"

#: FT10-yalarm2.sh:273
msgid "Duration"
msgstr "Времетраење"

#: FT10-yalarm2.sh:274
msgid "Interval"
msgstr "Интервал"

#: FT10-yalarm2.sh:275
msgid "Repeat"
msgstr "Повторете"

#: FT10-yalarm2.sh:276
msgid "seconds"
msgstr "секунди"

#: FT10-yalarm2.sh:277
msgid "infinite"
msgstr "бесконечна"

#: FT10-yalarm2.sh:280
msgid "Hour::CB"
msgstr "Час::CB"

#: FT10-yalarm2.sh:281
msgid "Minutes::CB"
msgstr "Минути::CB"
