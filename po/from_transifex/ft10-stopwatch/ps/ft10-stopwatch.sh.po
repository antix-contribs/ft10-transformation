# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Robin, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: 2022-01-18 00:37+0000\n"
"Last-Translator: Robin, 2022\n"
"Language-Team: Pashto (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ps/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ps\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: cli-stopwatch.sh:5
msgid "LAPS: Press Enter"
msgstr "LAPS: Enter ﺉړﮐﺭﻭ ﺭﺎﺸﻓ"

#: cli-stopwatch.sh:6
msgid "Exit: Close window"
msgstr "ﺉړﮐ ﻩﺪﻨﺑ ۍﮐړﮐ :ﻞﺗﻭ"
