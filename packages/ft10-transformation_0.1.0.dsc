Format: 3.0 (native)
Source: ft10-transformation
Binary: ft10-transformation
Architecture: all
Version: 0.1.0
Maintainer: PPC <ppc@ppc.ppc>
Homepage: https://gitlab.com/antix-contribs/ft10-transformation
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 10), gettext, bash, coreutils, util-linux, sed
Package-List:
 ft10-transformation deb x11 optional arch=all
Checksums-Sha1:
 d94a3f0bbacca7c4f949fc2c84bdb89645d2cc1f 21028 ft10-transformation_0.1.0.tar.xz
Checksums-Sha256:
 c21837c99692c873304f083062bd470b4c7b667534eafebe799d674817c49d5e 21028 ft10-transformation_0.1.0.tar.xz
Files:
 bd557d4a171d0a7c006accd79d464c52 21028 ft10-transformation_0.1.0.tar.xz
