#!/bin/bash
# script to automaticly configure a trash can in zzzfm, by PPC

#Start zzzfm so, if it was never run, it creates the default config file
zzzfm
#Make sure that  trash-cli is installed or check if the latest version is installed)
#Exit if trash-cli is not installed
	if ! [ -x "$(command -v trash)" ]; then
  	 x-terminal-emulator -T $"FT10" -e /bin/bash -c "gksu 'apt install -y trash-cli' && yad --center --title=TrashCan-Installer --window-icon='/usr/share/icons/papirus-antix/32x32/apps/gnome-debian.png' --fixed --width=300 --height=100 --image=/usr/share/icons/numix-square-antix/32x32/actions/gtk-ok.png  --text='Package(s) installed! ' --button='x'|| yad --center --title=FT10 --window-icon='/usr/share/icons/papirus-antix/32x32/apps/gnome-debian.png'  --fixed --width=300 --height=100 --image=/usr/share/icons/papirus-antix/32x32/emblems/emblem-rabbitvcs-modified.png --text=' Error installing the package! \n  Please read the log on the terminal window below  ' --button='x'"
     exit 1
    fi
    
#Recheck- if trash-cli is not installed, exit    
    	if ! [ -x "$(command -v trash)" ]; then
  	     exit 1
       fi

#If trash-cli is found, proceed with configuration:
#Close zzzfm, so we can edit it's config file
 pkill zzzfm
#back up original config file
 cp ~/.config/zzzfm/session  ~/.config/zzzfm/sessionBACKUP-FT10
#make insertions in zzzfm config file:
 echo "cstm_25a291d5-y="  >>~/.config/zzzfm/session  
 echo cstm_25a291d5-key=65535 >>~/.config/zzzfm/session 
 echo cstm_25a291d5-label=Enviar para a reciclagem >>~/.config/zzzfm/session  
 echo cstm_25a291d5-icon=gtk-delete >>~/.config/zzzfm/session  
 echo cstm_25a291d5-cxt=3%%%%%0%%%%%2%%%%%2%%%%%.local/share/Trash/files >>~/.config/zzzfm/session
 echo cstm_25a291d5-prev=edit_delete >>~/.config/zzzfm/session 
 echo 'cstm_25a291d5-line=folder=%d; if [[ ${folder} != *".local/share/Trash/files"* ]]; then trash %F; fi' >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task_err=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-task_out=1 >>~/.config/zzzfm/session  
 echo cstm_25a291d5-keep=1 >>~/.config/zzzfm/session  
 
#find out where the keybinding to Delete is, and then the line next to it:
 declare -i delete_key_line
 delete_key_line=$(grep -nr 'edit_delete-key=65535' ~/.config/zzzfm/session | cut -d: -f1)
 delete_key_line+=1
 #add this line in the line afte the edite_delete_key:
 sed -i "`echo $delete_key_line`i\\edit_delete-keymod=4\\" ~/.config/zzzfm/session
 #find out what's initially next to the delete key:
  originally_next_to_delete=$(grep 'edit_delete-next=' ~/.config/zzzfm/session| cut -d= -f2)
 #now try make the contents of this line point to the Trash entry;
  sed -i 's/edit_delete-next=.*/edit_delete-next=cstm_25a291d5/' ~/.config/zzzfm/session 

# If the edit_delete-next line does not exist, insert it:
###TODO: check if the field exists in the config file: 
 echo edit_delete-next=cstm_25a291d5 >>~/.config/zzzfm/session

 #now tell that what comes next to Trash was what originally was after Delete 
 #sed -i "s/cstm_25a291d5-next=.*/cstm_25a291d5-next='echo $originally_next_to_delete'/" ~/.config/zzzfm/session
 
 ###restaurar a configuração original
 ###cp ~/.config/zzzfm/sessionBACKUP-FT10 ~/.config/zzzfm/session   
 ######## pkill zzzfm &&  mv  ~/.config/zzzfm-2-2-22 ~/.config/zzzfm/
