#!/bin/bash
# Adds/Removes icons to the Tint2 toolbar(adding icons is done using the info from the app's .desktop file) 
# By PPC, 6/01/2021 adapted from many, many on-line examples, and from toolbar icon manager for icewm
# GPL licence - feel free to improve/adapt this script - but keep the lines about the license and author
###Strings to translate:

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=jgmenu-editor

window_title="Tint2 icons"
text_loading="Tint2 icons..."
#add_icons_text="Select icon to be added to the toolbar"
#remove_icons_text="Select icon to be removed from the toolbar"
move_icons_text="Select icon to be moved in the toolbar"
moving_text="Moving "
add_text="Add"
remove_text="Remove"
move_text="Move"
###
window_icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png"

#Export files, so then can be used in functions
export window_title add_icons_text remove_icons_text move_icons_text moving


delete_icon()
{

for (( ; ; ))
do

###Function to delete  icon
#criar lista de ícones:
grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
#remover o texto "launcher_item_app = ", 21 letters
(cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt
#apagar ficheiro temporiario
echo > /tmp/lista_apelativa.txt
#processar, linha a linha, a lista dos launchers existentes na barra
cat /tmp/launchers.txt | while read line; do
ico=$(grep -o -m 1 '^Icon=.*' $line | cut -d\= -f2) 
	if [ -z "$ico" ]
		then
		echo "This icon - $ico - was not found, probably the corresponding application was uninstalled, ignoring it"
		#just ignore it
		#delete that non existing application icon from toolbar
		#grep -v $ico ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 
	else
      #icon really exists, was not just left behind from a unistalled app, procced adding ico, name and .desktop file to list:
		echo $ico >> /tmp/lista_apelativa.txt
	#	nam=$(grep -o -m 1 '^Name=.*' $line | cut -d\= -f2) 
		nam=$(cat $line| grep -o -m 1 '^Name=.*' $line | cut -d\= -f2) 

########
#Get system language (to allow localization):
lang=$(locale | grep LANG | cut -d= -f2 | cut -d. -f1)
#hack to fix languages that are identified in .desktop files by only 2 characters, and not 4 (5 counting the _)
#comparing text that's before the "_" to the text that after that, converted to lower case, if it matches, use only the leters before the "_"
l1=$(echo $lang |cut -d_ -f1)
l2=$(echo $lang |cut -d_ -f2)
l2_converted=$(echo "${l2,,}")
l2_converted=$(echo $l2_converted| cut -d' ' -f2)
if [ $l1 = $l2_converted ]; then lang=$(echo $l1); fi


nam2=$(cat $line |grep ^"Name\[$lang\]" |cut -d\= -f2) 
#if the localized name exists, use that as name ( $nam variable)
[[ ! -z "$nam2" ]] &&  nam=$nam2

########
		echo $nam  >> /tmp/lista_apelativa.txt
		echo $line >> /tmp/lista_apelativa.txt
	fi
done
# remove empty lines:
sed -i '/^[[:space:]]*$/d' /tmp/lista_apelativa.txt
cat /tmp/lista_apelativa.txt
###Get selected: icon (displayed), app name, .desktop file (hidden- this is really what we want, to remove it from tint2rc file)
selection=$(yad --window-icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png" --center --height=600 --width=450 --title=$"Tint2 icons" --list --column=:IMG --column=$" " --column=Desktop:HD --button="-" < /tmp/lista_apelativa.txt)	
selection_to_delete=$(echo $selection|cut -d\| -f3)	

# if no application selected- avoids creating empty icon: (also allows to exit the infinit loop tha keeps respawning the selection window)
if [ -z "$selection" ]; then exit
fi

#delete application icon from toolbar
grep -v $selection_to_delete ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 

#instantly restart toolbar to show changes
pkill tint2; tint2 

#refresh list of launchers
echo > /tmp/lista_apelativa.txt
cat /tmp/launchers.txt | while read line; do
app_with_descri=$(grep -E $line ~/.apps.txt)
[ -z "$app_with_descri" ] && app_with_descri=$line
echo $app_with_descri >> /tmp/lista_apelativa.txt
done

#end initial infine loop (so window is always open after selection)
done
}		

move_icon()
{
#criar lista de ícones:
grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
#remover o texto "launcher_item_app = ", 21 letters
(cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt
#apagar ficheiro temporiario
echo > /tmp/lista_apelativa.txt
#processar, linha a linha, a lista dos launchers existentes na barra
cat /tmp/launchers.txt | while read line; do
ico=$(grep -o -m 1 '^Icon=.*' $line | cut -d\= -f2) 
	if [ -z "$ico" ]
		then
		echo "This icon - $ico - was not found, probably the corresponding application was uninstalled, ignoring it"
		#just ignore it
		#delete that non existing application icon from toolbar
		#grep -v $ico ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 
	else
      #icon really exists, was not just left behind from a unistalled app, procced adding ico, name and .desktop file to list:
		###echo $ico >> /tmp/lista_apelativa.txt
	#	nam=$(grep -o -m 1 '^Name=.*' $line | cut -d\= -f2) 
		nam=$(cat $line| grep -o -m 1 '^Name=.*' $line | cut -d\= -f2) 

########
#Get system language (to allow localization):
lang=$(locale | grep LANG | cut -d= -f2 | cut -d. -f1)
#hack to fix languages that are identified in .desktop files by only 2 characters, and not 4 (5 counting the _)
#comparing text that's before the "_" to the text that after that, converted to lower case, if it matches, use only the leters before the "_"
l1=$(echo $lang |cut -d_ -f1)
l2=$(echo $lang |cut -d_ -f2)
l2_converted=$(echo "${l2,,}")
l2_converted=$(echo $l2_converted| cut -d' ' -f2)
if [ $l1 = $l2_converted ]; then lang=$(echo $l1); fi


nam2=$(cat $line |grep ^"Name\[$lang\]" |cut -d\= -f2) 
#if the localized name exists, use that as name ( $nam variable)
[[ ! -z "$nam2" ]] &&  nam=$nam2

########
		
	fi
	##echo $ico @ $nam @ $line >> /tmp/lista_apelativa.txt
	  echo $nam @ $line >> /tmp/lista_apelativa.txt
done

#choose application to move
		selection=$(yad --window-icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png" --title="$window_title" --width=450 --height=480 --center --separator=" " --list  --column=$" "  < /tmp/lista_apelativa.txt --button="↔":4)

        EXEC=$(echo $selection|cut -d@ -f2)
       #Line=$(echo $selection|cut -d@ -f2)
       nome=$(echo $selection|cut -d@ -f1)

		#get line number(s) where the choosen application is
	#	x=$(echo $EXEC)
		#get last filed of the selected line in practice it's the path and file name of the .desktop file)
	#	x=$(echo $EXEC | awk '{print $NF}')
	Line=$(cat $HOME/.config/tint2/tint2rc| grep -n -m 1 $EXEC|cut -f1 -d: )

		 #get number of lines in file
	#	 number_of_lines=$(wc -l < $file)
		 
 #####Act on selection:
#only do something if a icon was selected :
if test -z "$EXEC" 
then
      yad --center --text="Nothing was selected"
    else

#this performs an infinite loop, so the Move window is ALWAYS open unless the user clicks "Cancel"
	while :
	do

nome=$(cat /tmp/lista_apelativa.txt| grep $EXEC|cut -d@ -f1 )
yad --window-icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png" --center --undecorated --title="$window_title" --text=$"   $nome" \
--button="←":2 \
--button="Ok":1 \
--button="→":3 
##exit ##########################for testing only!!!

foo=$?
##Line_to_the_left=line_number=$((line_number-1))
##line_number=$a

if [[ $foo -eq 1 ]]; then exit
fi

if [[ $foo -eq 2 ]]; then
#move icon to the left, by moving the line with the selected .desktop file line one line up:
file=$HOME/.config/tint2/tint2rc
line_number=$Line
First_line=$(awk -v search="launcher_item_app" '$0~search{print NR; exit}' ~/.config/tint2/tint2rc)
 if (( Line > First_line )); then
   sed  -i -e "$((line_number-1))i$(head -$line_number $file | tail -1)" -e "${line_number}d" "$file"
   #restart tint2
   pkill tint2; tint2 
   Line=$(($Line-1))   # update selected icon's position, just in case the user wants to move it again
 fi
fi

#move icon to the right:
if [[ $foo -eq 3 ]]; then
#Move selected line one line down:
file=$HOME/.config/tint2/tint2rc
##line_number=$Line
Last_line=$(cat ~/.config/tint2/tint2rc| grep -n '.desktop' | tail -n1 | cut -d: -f1)
 if (( Last_line > Line )); then
   sed -i -n "$Line{h;n;G};p" $HOME/.config/tint2/tint2rc
#restart tint2
   pkill tint2; tint2 
   Line=$(($Line+1))
 fi
fi

	done

fi ### ends if cicle that checks if user selected icon to move in the main Move icon window

	}	

add_icon()
{
#check if window with the title "tint2-wait" is showing, if so, close that window:
wmctrl -lp | awk '/tint2-wait/{print $3}' | xargs kill

#use app-select to get application to be added to the menu
add_icon=$(app-select --s)

#if no choice was made, exit
[ -z "$add_icon" ] && exit

#if a choice was made, process app-select's output
NOME=$(echo $add_icon| cut -d '|' -f2)
EXECperc=$(echo $add_icon| cut -d '|' -f3)
ICONE=$(echo $add_icon| cut -d '|' -f6)
add=$(echo $add_icon| cut -d '|' -f1)

#IF no selection was made, exit
[ -z "$add" ] && exit

#add line to toolbar
echo "launcher_item_app =" $add  >> ~/.config/tint2/tint2rc
#instantly restart toolbar to show changes
#killall -SIGUSR1 tint2
pkill tint2; tint2 
###END of Function to add a new icon
}

#display main window
export -f delete_icon add_icon move_icon
DADOS=$(yad --window-icon=$window_icon --paned --center --splitter="200" --title="$window_title" \
--form  \
--center \
--field=$"ADD ICON!add:FBTN" "bash -c add_icon" \
--field=$"MOVE ICON!gtk-go-back-rtl:FBTN" "bash -c move_icon" \
--field=$"REMOVE ICON!remove:FBTN" "bash -c delete_icon" \
--wrap --no-buttons)

### wait for a button to be pressed then perform the selected function
foo=$?

[[ $foo -eq 1 ]] && exit 0
