#!/bin/bash
# Adds/Removes icons to the Tint2 toolbar(adding icons is done using the info from the app's .desktop file) 
# By PPC, 6/01/2021 adapted from many, many on-line examples, and from toolbar icon manager for icewm
# GPL licence - feel free to improve/adapt this script - but keep the lines about the license and author
###Strings to translate:

export TEXTDOMAINDIR=/usr/share/locale
export TEXTDOMAIN=jgmenu-editor

window_title="Tint2 icons"
text_loading="Tint2 icons..."
#add_icons_text="Select icon to be added to the toolbar"
#remove_icons_text="Select icon to be removed from the toolbar"
move_icons_text="Select icon to be moved in the toolbar"
moving_text="Moving "
add_text="Add"
remove_text="Remove"
move_text="Move"
###
window_icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png"

#Export files, so then can be used in functions
export window_title add_icons_text move_icons_text moving

main_window()
{

#Warn user, if using the terminal:
echo "WARNING: This script is not meant to be run from inside a terminal emulator. Doing so implies that, if you change anything on tint2 toolbar and it gets restarted from inside this terminal window, once you close this terminal window the tint2 toolbar will also close and will have to be restarted manually."	

#Localizable strings:	
add_icon_button=$"ADD ICON!add:FBTN"	
add_icon_button=$(echo $add_icon_button| cut -d '!' -f 1)

remove_icon_button=$"REMOVE ICON!remove:FBTN"	
remove_icon_button=$(echo $remove_icon_button| cut -d '!' -f 1)

#move_left_button=$"MOVE LEFT"
move_left_button=$"MOVE ICON!gtk-go-back-rtl:FBTN"
move_left_button=$(echo $move_left_button| cut -d '!' -f 1)

#move_right_button=$"MOVE RIGHT"
move_right_button=$"MOVE ICON!gtk-go-back-rtl:FBTN"
move_right_button=$(echo $move_left_button| cut -d '!' -f 1)

window_text=$"Click $add_icon_button or click an icon and then the  action you want to perform on it \n If a feature fails to work, please close this window and reopen it again"

#Main part of the function:
#Close any previous instance of the main window?

for (( ; ; ))
do

###Function to select quick launch icon
#criar lista de ícones:
grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
#remover o texto "launcher_item_app = ", 21 letters
(cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt
#apagar ficheiro temporiario
echo > /tmp/lista_apelativa.txt
#processar, linha a linha, a lista dos launchers existentes na barra
cat /tmp/launchers.txt | while read line; do
ico=$(grep -o -m 1 '^Icon=.*' $line | cut -d\= -f2)

#find correct locale, so script can use .dekstop translated name
lang=$(locale | grep LANG= | cut -d= -f2| cut -d . -f1)
#	lang=fr # uncomment this line ONLY for testing in other language only (in French)
lowercase_lang="${lang,,}"
first_part=$(echo $lowercase_lang| cut -d_ -f1)
second_part=$(echo $lowercase_lang| cut -d_ -f2)
if [ "$first_part" == "$second_part" ]; then
	lang=$first_part
fi
#end  of find correct locale, so script can use .dekstop translated name
 
	if [ -z "$ico" ]
		then
		echo "This icon - $ico - was not found, probably the corresponding application was uninstalled, ignoring it"
		#just ignore it
		#delete that non existing application icon from toolbar
		#grep -v $ico ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 
	else
      #icon really exists, was not just left behind from a unistalled app, procced adding ico, name and .desktop file to list:
		echo $ico >> /tmp/lista_apelativa.txt
		 ### TO DO: get localized application name, used as i
		nam=$(grep -F -m 1 "Name[${lang}]=" $line | cut -d\= -f2) 
		if [ -z "$nam" ]; then
			#"No localized name foundm using the original one"
			nam=$(grep -F -m 1 "Name=" $line | cut -d\= -f2)
		fi
		
			echo $line = $nam
		
		echo $nam >> /tmp/lista_apelativa.txt
		echo $line >> /tmp/lista_apelativa.txt
	fi
		
done
# remove empty lines:
sed -i '/^[[:space:]]*$/d' /tmp/lista_apelativa.txt
###Get selected: icon (displayed), app name, .desktop file (hidden- this is really what we want, to remove it from tint2rc file)
yad --fixed --window-icon="/usr/share/icons/papirus-antix/24x24/apps/tint2.png" --center --height=600 --width=450 --title=$"Tint2 icons" --text="$window_text" --list --column=:IMG --column=$" " --column=Desktop:HD --button="+ $add_icon_button":"bash -c add_icon"  --button="← $move_left_button":"4" --button="→ $move_right_button":"66" --button="- $remove_icon_button":44 < /tmp/lista_apelativa.txt > /tmp/icon_selection

foo=$? && echo $foo > /tmp/yad

EXIT_CODE=$(cat /tmp/yad)  
export choice=$(cat /tmp/icon_selection)

####Process choice:

    #### if no application selected- avoids creating empty icon: (also allows to exit the infinit loop tha keeps respawning the selection window)
if [ -z "$choice" ]; then 
#yad --window-icon="/usr/share/icons/papirus-antix/24x24/apps/tint2.png"  --center --text="No valid selection or user closed the window. Exiting withour performing any change" --button=" x "
exit
fi

#yad --center --text="you selected to $EXIT_CODE on icon $choice" #for testing only
#exit  #for testing only

case $EXIT_CODE in
	232)
		yad --center --text="exting"
		exit
		;;
	232)
		yad --center --text="exting"
		exit
		;;
    4) #move selected icon to the left
        #Remove spaces in tint2 config file
        grep -v -x '[[:blank:]]*' ~/.config/tint2/tint2rc > /tmp/tint2rc.tmp && mv /tmp/tint2rc.tmp ~/.config/tint2/tint2rc
        icon_left
        ;;
    66) #move selected icon to the right
        #Remove spaces in tint2 config file
        grep -v -x '[[:blank:]]*' ~/.config/tint2/tint2rc > /tmp/tint2rc.tmp && mv /tmp/tint2rc.tmp ~/.config/tint2/tint2rc
        icon_right
        ;;
    44) #remove selected icon
    
		selection_to_delete=$(echo $choice|cut -d\| -f3)	
		#delete application icon from toolbar
		grep -v $selection_to_delete ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 

		#refresh list of launchers
		echo > /tmp/lista_apelativa.txt
		cat /tmp/launchers.txt | while read line; do
		app_with_descri=$(grep -E $line ~/.apps.txt)
		[ -z "$app_with_descri" ] && app_with_descri=$line
		echo $app_with_descri >> /tmp/lista_apelativa.txt
		done #???

		#instantly restart toolbar to show changes
		pkill tint2; tint2 &

		sleep 0.1
		#close yad window
		wmctrl -c $"Tint2 icons"
		# restart script, showing updated main window
		main_window 

        
        ;;
    *)
        #yad --center --text="No action was selected." --button=" x "
        wmctrl -l | grep -q 'Tint2 icons'|| main_window 
        ;;
esac

done

}


icon_left()
{
 selection_to_move=$(echo $choice|cut -d\| -f3)
 line_number=$(grep -n "$selection_to_move" ~/.config/tint2/tint2rc|cut -d\: -f1)
 first_line=$(grep -n "^launcher_tooltip" ~/.config/tint2/tint2rc|cut -d\: -f1)
 first_line=$((first_line + 1))
 # yad --center --text="moving $selection_to_move icon to the left. it is currently in line $line_number"

 # Check if the line number is greater than 1 to avoid moving the first line
 if (( line_number > $first_line )); then
	line_number=$((line_number - 1))
	sed -i -n "$line_number{h;n;G};p" ~/.config/tint2/tint2rc
	#restart tint2
	pkill tint2; tint2 &
	#end of test 
	main_window
 fi

}

icon_right()
{
 selection_to_move=$(echo $choice|cut -d\| -f3)
 line_number=$(grep -n "$selection_to_move" ~/.config/tint2/tint2rc|cut -d\: -f1)
 # Get the total number of lines in the file
	total_lines=$(wc -l < ~/.config/tint2/tint2rc)
	# Check if the line number is not the last line
	if [ "$line_number" -lt "$total_lines" ]; then
	 sed -i -n "$line_number{h;n;G};p" ~/.config/tint2/tint2rc
	 #restart tint2
	 pkill tint2; tint2 &
	 #end of test 
	 main_window
	else 	 #"This was already the last line of the file, its impossible to move it down"
 	 main_window
 fi 

}

add_icon()
{
#check if window with the title "tint2-wait" is showing, if so, close that window:
wmctrl -lp | awk '/tint2-wait/{print $3}' | xargs kill

#use app-select to get application to be added to the menu
add_icon=$(app-select --s)

#if no choice was made, exit
[ -z "$add_icon" ] && exit

#if a choice was made, process app-select's output
NOME=$(echo $add_icon| cut -d '|' -f2)
EXECperc=$(echo $add_icon| cut -d '|' -f3)
ICONE=$(echo $add_icon| cut -d '|' -f6)
add=$(echo $add_icon| cut -d '|' -f1| awk '{print $NF}')

#IF no selection was made, exit
[ -z "$add" ] && exit

#add line to toolbar
echo "launcher_item_app =" $add  >> ~/.config/tint2/tint2rc

#instantly restart toolbar to show changes
pkill tint2; tint2 &

sleep 0.2
#close yad window
wmctrl -c $"Tint2 icons"
sleep 0.2
# restart script
#main_window
main_window

###END of Function to add a new icon
}

#display main window
export -f add_icon main_window
#delete_icon
main_window
exit
