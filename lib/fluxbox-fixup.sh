#!/bin/bash

#if [ "$DESKTOP_SESSION_WM" = "fluxbox" ]; then 
    # Perform conf file changes
#    echo "session.screen0.allowRemoteActions: true" >> $HOME/.fluxbox/init
#    echo "session.screen0.toolbar.tools: rootmenu, iconbar, clock" >> $HOME/.fluxbox/init
#    echo "session.screen0.toolbar.visible: false" >> $HOME/.fluxbox/init
#    #this one is optinal, changes to "Numix" theme, with a more modern, dark look:
#    echo "session.styleFile: /usr/share/fluxbox/styles/Numix">> $HOME/.fluxbox/init
#    #
#    echo "[app] (name=tint2)" >> $HOME/.fluxbox/apps
#    echo "[Layer] {6}" >> $HOME/.fluxbox/apps
#    echo "[end]" >> $HOME/.fluxbox/apps
#    #
#    #~ sed '2a\
#    #~ $HOME/.fehbg & \
#    #~ ft10-start &
#    #~ ' < $HOME/.fluxbox/startup >/tmp/t2instal
#    #~ cp /tmp/t2instal $HOME/.fluxbox/startup &&
#    #
#    #make spacefm the default file manager:
#    if [ -e /usr/share/applications/spacefm.desktop ]; then
#        ln -sf /usr/share/applications/spacefm.desktop $HOME/.local/share/desktop-defaults/file-manager.desktop
#    #make zzzfm the default file manager:
#    elif [ -e /usr/share/applications/zzzfm.desktop ]; then
#        ln -sf /usr/share/applications/zzzfm.desktop $HOME/.local/share/desktop-defaults/file-manager.desktop
#    fi
#fi
