#!/bin/bash
# Small script to toogle tint2 toolbar auto hide
###
# The script was created by PPC

original_auto_hide=$(egrep "^autohide =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
SUB='0'

if [[ "$original_auto_hide" == *"$SUB"* ]]; then
  seleccao="1"
  else 
  seleccao="0"
fi

sed -i "/^autohide =/s/$original_auto_hide/ $seleccao/g" ~/.config/tint2/tint2rc

# restart tint2
	killall -9 tint2
	nohup tint2 &
	exit
