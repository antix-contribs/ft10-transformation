#!/bin/bash
# Show/hide window titles in tint2 toolbar 
# By PPC
# GPL licence - feel free to improve/adapt this script - but keep the lines about the license and author

if ! grep -q "task_maximum_size = 450 100" ~/.config/tint2/tint2rc; then
  echo Currently not showing titles
  #Show window titles:
    TASK_TITLES=$(egrep "^task_text =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
	sed -i "/^task_text =/s/$TASK_TITLES/ 1/g" ~/.config/tint2/tint2rc
	sed -i 's/task_maximum_size =.*/task_maximum_size = 450 100/' ~/.config/tint2/tint2rc
	# restart tint2
	killall -9 tint2
	nohup tint2 &
	exit
fi

if ! grep -q "task_maximum_size = 36 100/' ~/." ~/.config/tint2/tint2rc; then
  echo Currently showing titles  
  #Don't show window titles:
  TASK_TITLES=$(egrep "^task_text =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
	sed -i "/^task_text =/s/$TASK_TITLES/ 0/g" ~/.config/tint2/tint2rc
	sed -i 's/task_maximum_size =.*/task_maximum_size = 36 100/' ~/.config/tint2/tint2rc
	# restart tint2
	killall -9 tint2
	nohup tint2 &
	exit
  
fi
