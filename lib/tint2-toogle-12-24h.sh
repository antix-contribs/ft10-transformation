#!/bin/bash
# Small script to toogle tint2 clock between 12h/24h
###
# The script was created by PPC

original_time_format=$(egrep "^time1_format =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
SUB='%H'

if [[ "$original_time_format" == *"$SUB"* ]]; then
echo "currently showing 24h"
seleccao="%I:%M"
  else
echo "currently showing12h"   
seleccao="%H:%M"
fi

#Edit changes in tint2rc config file
sed -i "/^time1_format =/s/$original_time_format/ $seleccao/g" ~/.config/tint2/tint2rc

# restart tint2
killall -9 tint2
nohup tint2 &
exit
