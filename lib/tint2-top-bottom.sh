#!/bin/bash
# toggle tint2 toolbar between top/bottom position 
###
# The script was created by PPC
# Modified by marcelocripe 12/02/2021 - script prepared for translation

        TEXTDOMAINDIR=/usr/share/locale 
        TEXTDOMAIN=tint2-top-bottom


#Strings to translate:
window_title=$"Toolbar position"
text=$"Select Toolbar position"
top=$"top"
bottom=$"bottom"
left=$"left"
right=$"right"
###
window_icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png"
POSICAO_ATUAL=$(egrep "^panel_position =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
#echo $POSICAO_ATUAL
cima_ou_baixo=$(echo $POSICAO_ATUAL |cut -d' ' -f1)
#echo $cima_ou_baixo

seleccao=$(yad --window-icon=$window_icon --title="$window_title" --form --center --text=$"$window_title" --field=":CB" \!"$top"\!"$bottom"\!"$left"\!"$right")

seleccao=$(echo "${seleccao::-1}")


if [ "$seleccao" == "" ]; then exit
fi

##Use this variable substitution, handy even more, if the menu options are localized, to translate back to the original english settings
if [ "$seleccao" == $"bottom" ]; then seleccao="bottom center horizontal"
fi

if [ "$seleccao" == $"top" ]; then seleccao="top center horizontal"
fi

if [ "$seleccao" == $"left" ]; then seleccao="left center vertical"
fi

if [ "$seleccao" == $"right" ]; then seleccao="center right vertical"
fi

##Edit tint2 config file:
sed -i "/^panel_position =/s/$POSICAO_ATUAL/ $seleccao/g" ~/.config/tint2/tint2rc

# restart tint2
killall -9 tint2
nohup tint2 &
exit
