#!/bin/bash
# fonte

FONTE_ATUAL=$(egrep "^task_font =" ~/.config/tint2/tint2rc | cut -d'=' -f2)

while NOVA_FONTE=$( yad --font --title='Edit Tint2 Font' --fontname="$FONTE_ATUAL"\
					--button='X':1 --button='OK':0)
do
	sed -i "/^task_font =/s/$FONTE_ATUAL/ $NOVA_FONTE/g" ~/.config/tint2/tint2rc
	# reiniciar o tint2
	killall -9 tint2
	tint2 &
	FONTE_ATUAL=$(egrep "^task_font =" ~/.config/tint2/tint2rc | cut -d'=' -f2)
done


