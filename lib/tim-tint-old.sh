#!/bin/bash
# Adds/Removes icons to the Tint2 toolbar(adding icons is done using the info from the app's .desktop file) 
# By PPC, 6/01/2021 adapted from many, many on-line examples, and from toolbar icon manager for icewm
# GPL licence - feel free to improve/adapt this script - but keep the lines about the license and author
###Strings to translate:

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=icewm-toolbar-icon-manager.sh

window_title="Tint2 icons"
text_loading="Tint2 icons..."
#add_icons_text="Select icon to be added to the toolbar"
#remove_icons_text="Select icon to be removed from the toolbar"
move_icons_text="Select icon to be moved in the toolbar"
moving_text="Moving "
add_text="Add"
remove_text="Remove"
move_text="Move"
###

#Export files, so then can be used in functions
export window_title add_icons_text remove_icons_text move_icons_text moving

#Get system language (to allow localization):
lang=$(locale | grep LANG | cut -d= -f2 | cut -d_ -f1) #To test localization to another language, like french, use: lang=fr
window_icon="/usr/share/icons/papirus-antix/22x22/apps/tint2.png"
#Loop through all .desktop files in the applications folders and extract name and save that to a .txt file 

cd /usr/share/applications/
find ~+ -type f -name "*.desktop" > ~/.apps-antix-0.txt
sort ~/.apps-antix-0.txt > ~/.apps-antix.txt
cd ~

for file in $(cat ~/.apps-antix.txt)
do
 name1=$(grep -o -m 1 '^Name=.*' $file)
 icon0=$(grep -o -m 1 '^Icon=.*' $file)
ICON=$(echo $icon0 |cut -d= -f2)

###Start of get full path to the used icon:

 #try to find app icon:
	ICONwithoutpath=$(basename $ICON)

# By default set the icon as the gears icon, then look if the icon exist in several paths...
ICONE="/usr/share/icons/papirus-antix/24x24/apps/yast-runlevel.png"

# if a icon with a full path exists on the .desktop, use that icon
if [[ -f "$ICON00" ]]; then  ICONE=$ICON00
fi

#...Also check if the icon's name exists in several possible default paths, if a existing icon is found, use that instead!
#We can add as many paths as we want for the system to look for icons, also, we can look for icons with extensions other than .png (ex: svg), adding new "extension" and path's, and repeating the if-fi cicle
extension=".png"

path="/usr/share/pixmaps/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

path="/usr/share/icons/papirus-antix/48x48/apps/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

path="/usr/share/icons/papirus-antix/24x24/places/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

path="/usr/share/icons/papirus-antix/24x24/devices/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

path="/usr/share/icons/papirus-antix/24x24/mimetypes/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

## v.9 - looks in another folder, that has icon's for example, for Brave Browser
path="/usr/share/icons/hicolor/24x24/apps/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

## v.9 - looks in another folder, that has icon's for example, for Brave Browser
path="/usr/share/icons/hicolor/24x24/apps/"
if [[ -f "$path$ICONwithoutpath$extension" ]]; then  ICONE=$path$ICONwithoutpath$extension
fi

## v.9 – if no icon was found after searching the default couple of icon folders, perform active search for icons and use search result only if an icon was found- it takes about 1 second, but almost always finds a icon!
default="/usr/share/icons/papirus-antix/24×24/apps/yast-runlevel.png"
if [ "$ICONE" == "$default" ]; then
  search=$(locate /usr/share/icons/*/$ICONwithoutpath$extension)
  first_result=$(echo $search | head -n1 | awk '{print $1;}')
  if [ -z "$first_result" ]
  then 
  echo $"No icon located, using default Gears icon"
  else 
  echo $"Icon located!" ; ICONE=${first_result::-4}
  fi
fi
### End of get full path to the used icon
  
### localized menu entries generator (slows the script down, but produces nearly perfectly localized menus):
    name2=$name1
	translated_name1=$(grep -o -m 1 "^Name\[$lang\]=.*" $file)
	[ -z "$translated_name1" ] && note="No localized name found, using the original one" || name2=$translated_name1
	#if the desktop file has the string "Desktop Action" simply use the original untranslated name, to avoid risking using a translation that's not the name of the app
	grep -q "Desktop Action" $file && name2=$name1
	name1=$name2
 ### end of localized menu entries generator	 
 name=$(echo $name1|sed 's/.*\=//') 

echo "$name"  @ "$ICONE" @  "$file"
done > /tmp/list.txt
sort /tmp/list.txt > ~/.apps.txt
###
###devide one file per line, starting with icon, then name of app, then .desktop file:
###
while IFS=@  read name icon desktop_file; do
    echo $icon
    echo $name
    echo $desktop_file
done < "$HOME/.apps.txt" > "$HOME/.icon_apps.txt"

delete_icon()
{

for (( ; ; ))
do

###Function to delete  icon
#criar lista de ícones:
grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
#remover o texto "launcher_item_app = ", 21 letters
(cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt
#apagar ficheiro temporiario
echo > /tmp/lista_apelativa.txt
#processar, linha a linha, a lista dos launchers existentes na barra
cat /tmp/launchers.txt | while read line; do
ico=$(grep -o -m 1 '^Icon=.*' $line | cut -d\= -f2) 
	if [ -z "$ico" ]
		then
		echo "This icon - $ico - was not found, probably the corresponding application was uninstalled, ignoring it"
		#just ignore it
		#delete that non existing application icon from toolbar
		#grep -v $ico ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 
	else
      #icon really exists, was not just left behind from a unistalled app, procced adding ico, name and .desktop file to list:
		echo $ico >> /tmp/lista_apelativa.txt
		nam=$(grep -o -m 1 '^Name=.*' $line | cut -d\= -f2) 
		echo $nam >> /tmp/lista_apelativa.txt
		echo $line >> /tmp/lista_apelativa.txt
	fi
done
# remove empty lines:
sed -i '/^[[:space:]]*$/d' /tmp/lista_apelativa.txt
cat /tmp/lista_apelativa.txt
###Get selected: icon (displayed), app name, .desktop file (hidden- this is really what we want, to remove it from tint2rc file)
selection=$(yad --window-icon="" --center --height=600 --width=450 --title=$"Tint2 icons" --list --column=:IMG --column=$"Double click any Application to remove its icon:" --column=Desktop:HD --button="-" < /tmp/lista_apelativa.txt)	
selection_to_delete=$(echo $selection|cut -d\| -f3)	

# if no application selected- avoids creating empty icon: (also allows to exit the infinit loop tha keeps respawning the selection window)
if [ -z "$selection" ]; then exit
fi

#delete application icon from toolbar
grep -v $selection_to_delete ~/.config/tint2/tint2rc > temp && mv temp ~/.config/tint2/tint2rc		 

#instantly restart toolbar to show changes
old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &

#refresh list of launchers
echo > /tmp/lista_apelativa.txt
cat /tmp/launchers.txt | while read line; do
app_with_descri=$(grep -E $line ~/.apps.txt)
[ -z "$app_with_descri" ] && app_with_descri=$line
echo $app_with_descri >> /tmp/lista_apelativa.txt
done

#end initial infine loop (so window is always open after selection)
done
}		

move_icon()
{
	#criar lista de ícones:
grep -r "launcher_item_app" ~/.config/tint2/tint2rc > /tmp/launchers_list.txt
#remover o texto "launcher_item_app = ", 21 leters
(cat /tmp/launchers_list.txt | cut -c21-) > /tmp/launchers.txt

# estabelecer linha que é o limite minimo esquerdo até onde um ícone pode ser movido
limite_esq_zona=$(grep -n -m 1 "# Launcher" ~/.config/tint2/tint2rc |sed  's/\([0-9]*\).*/\1/')
limite_esq_launchers=$(grep -n -m 1 "launcher_item_app =" ~/.config/tint2/tint2rc |sed  's/\([0-9]*\).*/\1/')
if [ $limite_esq_launchers -ge $limite_esq_zona ]; then limite_esq=$limite_esq_launchers
 else
 limite_esq=$limite_esq_zona
fi
 limite_esq_final=$(($limite_esq -1 ))
 echo $limite_esq_final

# estabelecer linha que é o limite máximo direito até onde um ícone pode ser movido 
#number_of_lines=$(wc -l < ~/.config/tint2/tint2rc)
limite_dir_launchers=$(grep -n launcher_item_app ~/.config/tint2/tint2rc |cut -d':' -f1 |tail -n 1)
 
#apagar ficheiro temporiario
echo > /tmp/lista_apelativa.txt
#processar linha alinha a lista dos launchers existentes na barra
cat /tmp/launchers.txt | while read line; do
### ver se o ficheiros apps.txt, que tem todos os programas, com descrição, tem uma linha que corresponde à aplicação que está a ser analisada, a "line" da lista launchers.txt
app_with_descri=$(grep -E $line ~/.apps.txt)
#se não houver correspondencia, manter o nome do .desktop
[ -z "$app_with_descri" ] && app_with_descri=$line
echo $app_with_descri >> /tmp/lista_apelativa.txt
done

#choose application to move
		EXEC=$(yad --window-icon=$window_icon --title="$window_title" --width=450 --height=480 --center --separator=" " --list  --column=$"Double click any Application to move its icon:"  < /tmp/lista_apelativa.txt --button="↔":4)

		#get line number(s) where the choosen application is
		x=$(echo $EXEC)
		#get last filed of the selected line in practice it's the path and file name of the .desktop file)
		x=$(echo $EXEC | awk '{print $NF}')
		Line=$(grep -n -m 1 $x ~/.config/tint2/tint2rc |cut -f1 -d: )

		 #get number of lines in file
		 number_of_lines=$(wc -l < $file)
		 
 #####Act on selection:
#only do something if a icon was selected :
if test -z "$x" 
then
      echo "nothing was selected"
else

file_name=~/.config/tint2/tint2rc
a=$Line

#this performs an infinite loop, so the Move window is ALWAYS open unless the user clicks "Cancel"
	while :
	do

nome=$(echo $EXEC |cut -d'-' -f1)
yad --window-icon=/usr/share/pixmaps/icewm_editor.png --center --undecorated --title="$window_title" --text="$moving $nome" \
--button=" !/usr/share/icons/papirus-antix/22x22/actions/gtk-quit.png":1 \
--button="←":2 \
--button="→":3 

foo=$?
Line_to_the_left=line_number=$((line_number-1))
line_number=$a

if [[ $foo -eq 1 ]]; then exit
fi

#move icon to the left:
limite_esquerdo=0
if [[ $foo -eq 2 ]]; then
b=$(($a-1))
	if [ $b -gt $limite_esq_final ]; then
sed -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} > test2.txt
#create backup file before changes
cp ~/.config/tint2/tint2rc ~/.config/tint2/tint2rc.bak
 cp test2.txt ~/.config/tint2/tint2rc
 sleep .3

old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &

a=$(($a-1))   # update selected icon's position, just in case the user wants to move it again
	fi
fi

#move icon to the right:
if [[ $foo -eq 3 ]]; then
a=$(($a+1))
b=$(($a-1))
    if [ $b -ge  $limite_dir_launchers ]; then 
      exit 
  else
  sed -n "$b{h; :a; n; $a{p;x;bb}; H; ba}; :b; p" ${file_name} > test2.txt
#create backup file before changes 
cp ~/.config/tint2/tint2rc ~/.config/tint2/tint2rc.bak
    cp test2.txt  ~/.config/tint2/tint2rc
    sleep .3

old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &
# There's no need to update selected icon's position, just in case the user wants to move it again, because moving right just moves the icon to the right of the select icon to the left, so, it updates instantly the selected icon's position
  fi
fi

	done

fi ### ends if cicle that checks if user selected icon to move in the main Move icon window

	}	

add_icon()
{
for (( ; ; ))
do

###yad window to select icon to add to tint2 toolbar
selection=$(yad --center --height=600 --width=450 --window-icon=$window_icon --title=$"Choose application to add to the Toolbar" --list --column=:IMG --column=App --column=".desktop":HD --button="+"< $HOME/.icon_apps.txt)
add=$(echo $selection| cut -f3 -d"|")

#IF no selection was made, exit
[ -z "$selection" ] && exit

#add line to toolbar
echo "launcher_item_app =" $add  >> ~/.config/tint2/tint2rc
#instantly restart toolbar to show changes
old_tint2_pid=$(pgrep tint2)
nohup  tint2 && sleep 0.1 &&
kill -9  $old_tint2_pid &

done
###END of Function to add a new icon
}

##check if window with the title "tint2-wait" is showing, if so, close that window:
wmctrl -lp | awk '/tint2-wait/{print $3}' | xargs kill

#display main window
export -f delete_icon add_icon move_icon
DADOS=$(yad --window-icon=$window_icon --paned --center --splitter="200" --title="$window_title" \
--form  \
--center \
--field=$"ADD ICON!add:FBTN" "bash -c add_icon" \
--field=$"MOVE ICON!gtk-go-back-rtl:FBTN" "bash -c move_icon" \
--field=$"REMOVE ICON!remove:FBTN" "bash -c delete_icon" \
--wrap --no-buttons)

### wait for a button to be pressed then perform the selected function
foo=$?

[[ $foo -eq 1 ]] && exit 0
