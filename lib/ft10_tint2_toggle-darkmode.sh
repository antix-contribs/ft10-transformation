#!/bin/bash
# Tint2 toolbar quick settings- toggle dark mode, by PPC
FILE="$HOME/.config/tint2/tint2rc"
STRING="background_color = #000000 95   #ft10 toolbar background"
if  grep -q "$STRING" "$FILE" ; then
         echo Currently in dark mode, entering light mode ;
         sed -i 's*background_color = \#000000 95   \#ft10 toolbar background*background_color = \#808090 95   \#ft10 toolbar background*' $FILE 
 else
         echo Currently in light mode, entering dark mode ;
         sed -i 's*background_color = \#808090 95   \#ft10 toolbar background*\background_color = \#000000 95   \#ft10 toolbar background*' $FILE
fi
# restart tint2
killall -9 tint2
nohup tint2 &
