��          T      �       �      �      �      �   
   �   k        p  �  �          *     F     U  }   a     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Icelandic (https://www.transifex.com/antix-linux-community-contributions/teams/120110/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD tímabelti FT10 klukka Settu inn tímabelti til að bæta við heimsklukkuna \n (Sláðu ekkert inn til að leita í gegnum öll tiltæk tímabelti) Fjarlægja tímabelti 