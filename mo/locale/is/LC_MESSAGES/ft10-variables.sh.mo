��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P  
   �     �     �     �  
                  !     )     7  8   >  
   w     �     �     �     �  j   �  
             #     8  
   A     L     S     Z  
   z     �     �     �     �  
   �     �     �     �     �     �      	     	     	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Icelandic (https://www.transifex.com/antix-linux-community-contributions/teams/120110/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
 Aukahlutir Allt Hljóð Vafri Reiknivél Klukka Stjórnstöð Þróun Tölvupóstur Hætta Uppáhaldsforrit: (smelltu hér til að breyta listanum) Skráasafn Skrár Leikir Grafík Internet Vinstri smellur- Bæta við, færa, fjarlægja tækjastiku tákn; Hægri smelltu - Stjórna tækjastikunni Matseðill Margmiðlun Netstjóri - Connman Fréttir Skrifstofa Myndir Nýleg Leitaðu að forritum og skrám Stillingar Sýna skjáborð Stöðva app Kerfi Verkefnaskipti Flugstöð Texti Textaritill Taktu USB drif úr sambandi Myndband Bindi Veður Veður Vefskoðari 