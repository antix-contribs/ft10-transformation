��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  r  �  (   #  '   L  6   t  '   �     �  G   �     .     =     E     X     k     �  	   �     �  
   �     �  .   �  3   �  	             	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Samoan (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sm/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sm
Plural-Forms: nplurals=1; plural=0;
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    E te mautinoa e seti le fa'ailo mo lena taimi taeao? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alalaga Ua seti le alarm i E moʻi lava oe??? Fa'aleaogaina le Fa'ailoga Umi Itula::CB Vaeluaga minute::CB Toe fai Ina ia faalēaogāina, kiliki-matau mo le lisi Ina ia faalēaogāina, kiliki-matau icon mo le lisi e le gata sekone 