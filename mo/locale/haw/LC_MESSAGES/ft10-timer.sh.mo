��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  }  y     �  D     *   R  H   }     �     �     �     �  %     (   )     R     W  1   d  0   �     �  7   �          2     H     O  =   a  6   �     �     �     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Hawaiian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/haw/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: haw
Plural-Forms: nplurals=2; plural=(n != 1);
 inoa faila kani alarm E noi e hoʻomaka i kēlā me kēia hoʻonohonoho (nā manawa āpau) E noi e hoʻomaka i kēlā me kēia manawa Hoʻopaʻa ʻakomi i ka pahu holomua i ka wā e pau ai nā hoʻonohonoho Hoʻonohonoho Ka lōʻihi Pau kēlā me kēia manawa Ua pau. ʻIkepili i ka hōʻailona Sysmonitor Nā hoʻonohonoho manawa manawa lehulehu Inoa ʻAʻole loa Ka helu o nā manawa e holo ai (nā manawa āpau) Memo pop-up ke pau kēlā me kēia hoʻonohonoho Pop-up memo ke pau ka manawa Hōʻano hou ka Bar Progress i kēlā me kēia x kekona Mākaukau e hoʻomaka HOOPAA I KA KOMPUTERE Kekona Kekona!Nā minuke Hoʻokani kani i ka pau ʻana o kēlā me kēia hoʻonohonoho Hoʻokani kani i ka pau ʻana o kēlā me kēia manawa Nā ʻāpana manawa manawa Nā hola manawa ua pau. 