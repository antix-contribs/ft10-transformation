��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y  5   �  t   *  [   �  �   �  !   �     �  ;   �     �  S     ;   d  	   �     �  Z   �  _   	  e   	  v   �	  8   \
  /   �
     �
  "   �
  a   �
  g   Z  ;   �     �             	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Marathi (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mr
Plural-Forms: nplurals=2; plural=(n != 1);
 अलार्म आवाज फाइलनाव प्रत्येक सेट सुरू करण्यास सांगा (सर्व टाइमर) प्रत्येक टाइमर सुरू करण्यास सांगा सर्व सेट संपल्यावर प्रोग्रेस बार डिस्प्ले ऑटो बंद करा कॉन्फिगरेशन कालावधी प्रत्येक टाइमर समाप्त संपले. सिस्मॉनिटर इंडिकेटरचा इंटरफेस एकाधिक टाइमर सेटिंग्ज नाव कधीच नाही सेट चालवण्याच्या वेळा (सर्व टाइमर) प्रत्येक सेट संपल्यावर पॉप-अप संदेश प्रत्येक टाइमर संपल्यावर पॉप-अप संदेश प्रोग्रेस बार प्रत्येक x सेकंदांनी अपडेट करा सुरू करण्यासाठी सज्ज संगणक निलंबित करा सेकंद सेकंद!मिनिटे प्रत्येक सेट संपल्यावर अलार्म वाजवा प्रत्येक टाइमर संपल्यावर अलार्म वाजवा टाइमर कालावधी युनिट्स टाइमर संपले आहे. 