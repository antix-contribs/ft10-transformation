��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  z  �  (   +  '   T  �   |  '   2     Z  G   m     �  	   �  )   �  ,   �  )   %     O     e     s     �     �  l   �  �   -     �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Marathi (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mr
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    तुम्ही उद्या त्या वेळेसाठी अलार्म सेट करणार असल्याची खात्री आहे का? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title गजर अलार्म सेट केला तुला खात्री आहे??? अलार्म रद्द करा कालावधी तास::CB मध्यांतर मिनिटे::CB पुन्हा करा रद्द करण्यासाठी, मेनूसाठी उजवे-क्लिक करा रद्द करण्यासाठी, मेनूच्या चिन्हावर उजवे-क्लिक करा अनंत सेकंद 