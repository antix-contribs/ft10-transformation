��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P                 
   %  
   0     ;     ?     P     W     ^  ;   d     �     �     �     �     �  f   �     <     E     Q     m     u     z     �     �     �     �     �     �     �     �     	     		     	     2	     8	     @	     H	     P	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Croatian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hr
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Pribor svi Audio Preglednik Kalkulator Sat Kontrolni centar Razvoj e-mail Izlaz Omiljene aplikacije: (kliknite ovdje za uređivanje popisa) Upravitelj datotekama Datoteke Igre Grafika Internet Lijevi klik - Dodaj, premjesti, ukloni ikone na alatnoj traci; Desni klik - Upravljanje alatnom trakom izbornik Multimedija Upravitelj mreže - Connman Vijesti Ured Slike Nedavno Tražite programe i datoteke Postavke Prikaži radnu površinu Zaustavite aplikaciju Sustav Prebacivanje zadataka Terminal Tekst Uređivač teksta Isključite USB pogone Video Volumen Vrijeme Vrijeme Web-preglednik 