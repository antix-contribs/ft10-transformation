��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  |  y  B   �  �   9  `   �  �   ,  !   �       G   2     z  �   �  P   	     k	  '   x	  �   �	  �   :
  �   �
  �   E  :   �  e     !   l  :   �  v   �  s   @  >   �     �     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Malayalam (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ml/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ml
Plural-Forms: nplurals=2; plural=(n != 1);
 അലാറം ശബ്ദ ഫയലിന്റെ പേര് ഓരോ സെറ്റും ആരംഭിക്കാൻ ആവശ്യപ്പെടുക (എല്ലാ ടൈമറുകളും) ഓരോ ടൈമറും ആരംഭിക്കാൻ ആവശ്യപ്പെടുക എല്ലാ സെറ്റുകളും അവസാനിക്കുമ്പോൾ പ്രോഗ്രസ് ബാർ ഡിസ്പ്ലേ സ്വയമേവ അടയ്ക്കുക കോൺഫിഗറേഷൻ ദൈർഘ്യം ഓരോ ടൈമറും അവസാനിക്കുന്നു തീർന്നു. സിസ്u200cമോണിറ്റർ ഇൻഡിക്കേറ്ററിലേക്കുള്ള ഇന്റർഫേസ് ഒന്നിലധികം ടൈമർ ക്രമീകരണങ്ങൾ പേര് ഒരിക്കലുമില്ല പ്രവർത്തിപ്പിക്കേണ്ട സമയങ്ങളുടെ എണ്ണം (എല്ലാ ടൈമറുകളും) ഓരോ സെറ്റും അവസാനിക്കുമ്പോൾ പോപ്പ്-അപ്പ് സന്ദേശം ഓരോ ടൈമറും അവസാനിക്കുമ്പോൾ പോപ്പ്-അപ്പ് സന്ദേശം ഓരോ x സെക്കൻഡിലും പ്രോഗ്രസ് ബാർ അപ്ഡേറ്റ് ചെയ്യുക ആരംഭിക്കാൻ തയ്യാറാണ് കമ്പ്യൂട്ടർ താൽക്കാലികമായി നിർത്തുക സെക്കന്റുകൾ സെക്കന്റുകൾ!മിനിറ്റ് ഓരോ സെറ്റും അവസാനിക്കുമ്പോൾ അലാറം മുഴക്കുക ഓരോ ടൈമറും അവസാനിക്കുമ്പോൾ അലാറം മുഴക്കുക ടൈമർ ദൈർഘ്യ യൂണിറ്റുകൾ ടൈമറുകൾ അവസാനിച്ചു. 