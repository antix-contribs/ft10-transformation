��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  =   �  R   1  >   �  x   �     <     O     \     |  6   �  0   �     �     	  \   "  W     ]   �  D   5	  -   z	      �	     �	     �	  S   �	  Y   B
  +   �
     �
  %   �
        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Sindhi (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sd/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sd
Plural-Forms: nplurals=2; plural=(n != 1);
 ﻮﻟﺎﻧ ﻮﺟ ﻞﺋﺎﻓ ﺯﺍﻭﺁ ﻡﺭﻻ﻿ﺍ (ﺮﻤﺋﺎٽ ﭛﺳ) ﻮﮁﭘ ِءﻻ﻿ ﮠﺮڪ ﻉﻭﺮﺷ ٽﻴﺳ ﺮﻫ ﻮﮁﭘ ءﻻ﻿ ﮠﺮڪ ﻉﻭﺮﺷ ﺮﻤﺋﺎٽ ﺮﻫ ﻲﭸﻭ ﻲﭤ ﻢﺘﺧ ٽﻴﺳ ﭛﺳ ﻦﻫڏﺟ ﻲﻠﭙﺳڊ ﺭﺎﺑ ﻲﻗﺮﺗ ﺪﻨﺑ ﺭﺎڪﺩﻮﺧ ڙﻮﺟ ﮪﺎﭠ ﻮﺻﺮﻋ ﻢﺘﺧ ﺮﻤﺋﺎٽ ﺮﻫ .ﻮﻴﭤ ﻭﺭﻮﭘ ﻭﺭﺎﺷﺍ Sysmonitor ﻲﮐ ﺲﻴﻓﺮٽﻧﺍ ﻥﻮﮕﻨٽﻴﺳ ﺮﻤﺋﺎٽ ﻦﮣﮭﮔ ﻮﻟﺎﻧ ﻪﻧ ﻪﺑ ﻦﻫڏڪ (ﺮﻤﺋﺎٽ ﭛﺳ) ﺩﺍﺪﻌﺗ ﻮﺟ ﺖﻗﻭ ِءﻻ﻿ ﮡﺋﻼ﻿ﻫ ٽﻴﺳ ﻲﭤ ﻲﺌﭤ ﻢﺘﺧ ٽﻴﺳ ﺮﻫ ﻦﻫڏﺟ ﻡﺎﻐﻴﭘ ﭖﺍ ﭖﺎﭘ ﻮﭤ ﻲﺌﭤ ﻢﺘﺧ ﺮﻤﺋﺎٽ ﺮﻫ ﻦﻫڏﺟ ﻡﺎﻐﻴﭘ ﭖﺍ ﭖﺎﭘ ڊﻨڪﻴﺳ x ﺮﻫ ٽﻳڊﭘﺍ ﺭﺎﺑ ﺲﻳﺮﮔﻭﺮﭘ ﺭﺎﻴﺗ ِءﻻ﻿ ﮠﺮڪ ﻉﻭﺮﺷ ﺮٽﻮﻴﭙﻤڪ ﻞﻄﻌﻣ ڊﻨڪﻴﺳ ڊﻨڪﻴﺳ!ٽﻨﻣ ﻲﭤ ﻲﺌﭤ ﻢﺘﺧ ٽﻴﺳ ﺮﻫ ﻦﻫڏﺟ ﻡﺭﻻ﻿ﺍ ﺯﺍﻭﺁ ﻮﭤ ﻲﺌﭤ ﻢﺘﺧ ﺮﻤﺋﺎٽ ﺮﻫ ﻦﻫڏﺟ ﻡﺭﻻ﻿ﺍ ﺯﺍﻭﺁ ٽﻧﻮﻳ ﺕﺪﻣ ﻲﺟ ﺮﻤﺋﺎٽ ﺮﻤﺋﺎٽ .ﻲﻫﺁ ﻮﻳﻭ ﻲﭤ ﻢﺘﺧ 