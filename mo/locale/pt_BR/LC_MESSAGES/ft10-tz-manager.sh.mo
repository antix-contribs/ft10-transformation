��          T      �       �      �      �      �   
   �   k        p  �  �     .     D  "   `     �  �   �      (                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2022
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADICIONAR o(s) Fuso(s) Horário(s) FT10 - Relógio Insira o Fuso Horário (em idioma Inglês) a ser adicionado ao Relógio Mundial \n (Não insira nada para ver todos os fusos horários disponíveis) REMOVER o(s) Fuso(s) Horário(s) 