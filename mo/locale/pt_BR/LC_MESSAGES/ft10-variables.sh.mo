��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P     �     	                    *     3     F     V     ]  8   z     �     �     �  	   �     �  �   �     �     �  0   �  	   �     �     �     �  +   �     *	  1   :	     l	     |	     �	     �	     �	     �	     �	     �	  �   �	     `
     s
     y
                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2022
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 Acessórios Tudo Áudio Internet Calculadora Relógio Centro de Controle Desenvolvimento E-mail Sair - Desligar ou Reiniciar Aplicativos Favoritos: (Clique aqui para editar a lista) Gerenciador de Arquivos Arquivos Jogos Gráficos Internet Clique com o Botão Esquerdo para Adicionar, Mover, Remover Ícones ou Clique com o Botão Direito para acessar as Configurações da Barra de Ferramentas FT10 Menu Iniciar Multimídia Gerenciador de Conexões de Redes - CMST Connman Notícias Escritório Imagens Recentes Pesquisar Aplicativos ou Pesquisar Arquivos Configurações Minimizar as Janelas e Exibir a Área de Trabalho Fechar a Janela Sistema Alternar Entre as Janelas Terminal Texto Editor de Texto Remover Dispostivos USB Vídeo Clique com o Botão Esquerdo para Controlar o Volume ou Clique com o Botão Direito para acessar o AlsaMixer - Misturador de Áudio Previsão do Tempo Clima Navegador de Internet 