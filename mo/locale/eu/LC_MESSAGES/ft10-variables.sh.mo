��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  y  P  
   �     �     �     �     �     �                    1  ;   7     s     �     �  	   �     �  t   �     (  
   .     9     U     ]  	   e     o      {  	   �     �     �     �     �  	   �     �     �     	     &	     -	  	   6	  	   @	     J	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Basque (https://www.transifex.com/antix-linux-community-contributions/teams/120110/eu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eu
Plural-Forms: nplurals=2; plural=(n != 1);
 Osagarriak Denak Audioa Arakatzailea Kalkulagailua Erlojua Kontrol Zentroa Garapena Posta elektronikoa Irten Aplikazio gogokoenak: (egin klik hemen zerrenda editatzeko) Fitxategien kudeatzailea Fitxategiak Jolasak Grafikoak Internet Ezkerreko klik- Gehitu, mugitu, kendu Tresna-barrako ikonoak; Egin klik eskuineko botoiarekin - Kudeatu tresna-barra Menua Multimedia Sare-kudeatzailea - Connman Berriak Bulegoa Argazkiak Duela gutxi Bilatu programak eta fitxategiak Ezarpenak Erakutsi mahaigaina Gelditu aplikazioa Sistema Zereginen aldatzailea Terminala Testua Testu-editorea Deskonektatu USB unitateak Bideoa Bolumena Eguraldia Eguraldia Web arakatzailea 