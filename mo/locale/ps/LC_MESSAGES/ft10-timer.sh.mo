��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  0   �  n   $  N   �  �   �     �     �     �     �  H   �  4   !  	   V     `  y   y  d   �  k   X	  _   �	  1   $
  %   V
     |
     �
  f   �
  l     4   �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Pashto (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ps/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ps
Plural-Forms: nplurals=2; plural=(n != 1);
 ﻡﻮﻧ ﻞﯾﺎﻓ ږﻏ ﻡﺭﻻ﻿ﺍ ﺩ (ﻪﻧﻭﺮﻤﯾﺎټ ﻝﻮټ) ﺉړﮐﻭ ﻪﻨﺘښﻮﻏ ﻮﻟﻮﮐ ﻞﯿﭘ ټﯿﺳ ﯤﺮﻫ ﺩ ﺉړﮐﻭ ﻪﻨﺘښﻮﻏ ﻮﻟﻮﮐ ﻞﯿﭘ ﺮﻤﯾﺎټ ﺮﻫ ﺩ ﺉړﮐ ﺪﻨﺑ ﺕﺎﻣﻮﺗﻭﺍ ﻪﻧﺩﻮښ ﺭﺎﺑ ګﺘﺨﻣﺮﭘ ﺩ ﻱږﯿﺳﺭﻭ ﻪﺗ ﯼﺎﭘ ﻪﻧﻮټﯿﺳ ﻝﻮټ ﯥﭼ ﻪﻠﮐ ﺐﯿﺗﺮﺗ ﻩﺩﻮﻣ ﯼﺎﭘ ﺮﻤﯾﺎټ ﺮﻫ .ﻮﺷ ﻢﺘﺧ ﺲﯿﻓﺮﭩﻧﺍ ﻪﺗ ﺺﺧﺎﺷ ﺮټﯿﻧﻮﻤﺴﯿﺳ ﺩ ﺕﺎﻤﯿﻈﻨﺗ ﺮﻤﯾﺎټ ﯼﺮﯾډ ﺩ ﻡﻮﻧ ﻪﻧ ﻪﻠﮑڅﯿﻫ (ﻪﻧﻭﺮﻤﯾﺎټ ﻝﻮټ) ﺮﯿﻤﺷ ﻮﻧﻮﺘﺧﻭ ﻮﻠﮐﺎټ ﺩ ﻩﺭﺎﭙﻟ ﻮﻟﻮﻠﭼ ﺩ ﻱږﯿﺳﺭﻭ ﻪﺗ ﯼﺎﭘ ټﯿﺳ ﺮﻫ ﯥﭼ ﻪﻠﮐ ﻡﺎﻐﯿﭘ ﭖﺍ ﭖﺎﭘ ﻱږﯿﺳﺭ ﻪﺗ ﯼﺎﭘ ﺮﻤﯾﺎټ ﺮﻫ ﯥﭼ ﻪﻠﮐ ﻡﺎﻐﯿﭘ ﭖﺍ ﭖﺎﭘ ﺩ ﻱږﯿﮐ ﻩﺯﺎﺗ ﯥﮐ ﻮﯿﻧﺎﺛ x ﺮﻫ ﻪﭘ ﺭﺎﺑ ګﺘﺨﻣﺮﭘ ﺩ ﯼﺩ ﻮﺘﻤﭼ ﻩﺭﺎﭙﻟ ﻞﯿﭘ ﺩ ﻝﻭډﻨځﻭ ﺮټﻮﻴﭙﻤﮐ ﯥﯿﻧﺎﺛ ﯥﯿﻧﺎﺛ!ﯥﻘﯿﻗﺩ ﻱږﯿﺳﺭﻭ ﻪﺗ ﯼﺎﭘ ټﯿﺳ ﺮﻫ ﯥﭼ ﻪﻠﮐ ﺉړﮐ ږﻏ ﻡﺭﻻ﻿ﺍ ﺩ ﻱږﯿﺳﺭﻭ ﻪﺗ ﯼﺎﭘ ﺮﻤﯾﺎټ ﺮﻫ ﯥﭼ ﻪﻠﮐ ﺉړﮐ ږﻏ ﻡﺭﻻ﻿ﺍ ﺩ ﻪﻧﻭﺪﺣﺍﻭ ﯤﺩﻮﻣ ﺩ ﺖﺧﻭ ﺩ ﻪﻧﻭﺮﻤﯾﺎټ ﯼﺩ ﯼﻮﺷ ﻢﺘﺧ 