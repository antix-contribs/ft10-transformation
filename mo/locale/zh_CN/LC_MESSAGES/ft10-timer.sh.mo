��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     �  '        9  0   U     �     �     �     �     �     �     �     �  '   �     !  $   =     b     �     �     �  
   �     �  $   �     �  	                	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Chinese (China) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 报警声音文件名 要求开始每组（所有计时器） 要求开始每个计时器 所有集合结束时自动关闭进度条显示 配置 期间 每个定时器结束 完成的。 Sysmonitor 指示器的接口 多个定时器设置 名称 绝不 运行次数设置（所有计时器） 每组结束时弹出消息 每个计时器结束时弹出消息 进度条每 x 秒更新一次 准备开始 暂停电脑 秒 秒!分钟 每组结束时发出警报 每个计时器结束时发出警报 计时器持续时间单位 计时器 结束了。 