��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  x  P  *   �     �     
       '   *     R  7   k     �     �     �  �   �  +   �     �  '   �     	     2	  �   K	     F
  !   _
  O   �
     �
     �
     �
       l         �  =   �  7   �     "     8  !   U  	   w  "   �  G   �     �               (     ;                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Tamil (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ta/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ta
Plural-Forms: nplurals=2; plural=(n != 1);
 துணைக்கருவிகள் அனைத்து ஆடியோ உலாவி கால்குலேட்டர் கடிகாரம் கட்டுப்பாட்டு மையம் வளர்ச்சி மின்னஞ்சல் வெளியேறு பிடித்த பயன்பாடுகள்: (பட்டியலைத் திருத்த இங்கே கிளிக் செய்யவும்) கோப்பு மேலாளர் கோப்புகள் விளையாட்டுகள் கிராபிக்ஸ் இணையதளம் இடது கிளிக்- சேர், நகர்த்த, கருவிப்பட்டி ஐகான்களை அகற்று; வலது கிளிக் - கருவிப்பட்டியை நிர்வகி பட்டியல் மல்டிமீடியா நெட்வொர்க் மேலாளர் - கான்மேன் செய்தி அலுவலகம் படங்கள் சமீப நிரல்கள் மற்றும் கோப்புகளைத் தேடுங்கள் அமைப்புகள் டெஸ்க்டாப்பைக் காட்டு பயன்பாட்டை நிறுத்து அமைப்பு பணி மாற்றி முனையத்தில் உரை உரை திருத்தி USB டிரைவ்களை துண்டிக்கவும் காணொளி தொகுதி வானிலை வானிலை இணைய உலாவி 