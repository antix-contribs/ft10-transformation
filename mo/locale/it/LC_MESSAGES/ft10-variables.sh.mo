��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P  	   �     �     �     �     �               !     *     1  ;   6     r     �     �     �     �  }   �       
   #     .     H     P     X     ]     e     }     �     �     �     �  	   �     �     �     �     �      	     	     	     	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Massimo de Barberis, 2022
Language-Team: Italian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
 Accessori Tutti Audio Browser Calcolatrice Orologio Centro di Controllo Sviluppo E-mail Esci Applicazioni Preferite: (click qui per modificare l'elenco) Gestore di File Files Giochi Grafica Internet Click sinistro: aggiunge,muove,rimuove le icone della barra degli strumenti; 
Click destro: gestisce la barra degli strumenti Menu Multimedia Gestore delle Connessioni Notizie Ufficio Foto Recenti Cerca programmi e files Impostazioni Mostra Scrivania Stop App Sistema Commutatore di attività Terminale Testo Editore di Testo Scollega unità USB Video Volume Meteo Meteo Navigatore Web 