��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  !   Q  5   s  %   �  o   �     ?     P     ]     v      �  $   �     �     �  C   �  5   '  ;   ]  6   �     �     �  
   �     	  3   	  9   O	     �	     �	     �	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Hebrew (https://www.transifex.com/antix-linux-community-contributions/teams/120110/he/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 הקעזא לילצ ץבוק םש (םירמייטה לכ) טס לכ ליחתהל שקב רמייט לכ ליחתהל ושקב םימייתסמ םיטסה לכ רשאכ תומדקתהה לגרס תגוצת לש תיטמוטוא הריגס הָרּוצְת ךֶׁשֶמ רמגנ רמייט לכ .רּומָג Sysmonitor ןווחמל קשממ תובורמ רמייט תורדגה םֵׁש אל םלועל (םירמייטה לכ) רדגומ הצירל םימעפה רפסמ םייתסמ טס לכ רשאכ תצפוק העדוה םייתסמ רמייט לכ רשאכ תצפוק העדוה תוינש x לכ תומדקתהה לגרס ןוכדע ליחתהל ןכומ בשחמ תייעשה תוינש תוינש!תוקד םייתסמ טס לכ רשאכ הקעזא עמשה םייתסמ רמייט לכ רשאכ הקעזא עמשה רמייט ךשמ תודיחי םירמייט .םייתסה 