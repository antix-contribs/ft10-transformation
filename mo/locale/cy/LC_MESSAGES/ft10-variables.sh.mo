��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P  	   �                               $  
   3     >     E  +   M     y     �     �     �  	   �  f   �       
        "  	   ?     I     Q     Y     b  
   �     �     �     �     �  	   �     �     �     �     �      	     	     	  	   	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Welsh (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cy
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
 Ategolion I gyd Sain Porwr Cyfrifiannell Cloc Canolfan Reoli Datblygiad E-bost Ymadael Hoff Apiau: (cliciwch yma i olygu'r rhestr) Rheolwr Ffeil Ffeiliau Gemau Graffeg Rhyngrwyd Cliciwch ar y chwith - Ychwanegu, symud, dileu eiconau Bar Offer; Cliciwch ar y dde - Rheoli Bar Offer Bwydlen Amlgyfrwng Rheolwr rhwydwaith - Connman Newyddion Swyddfa Lluniau diweddar Chwilio am raglenni a ffeiliau Gosodiadau Dangos Bwrdd Gwaith Stop App System Newidiwr tasgau Terfynell Testun Golygydd Testun Dad-blygio gyriannau USB Fideo Cyfrol Tywydd Tywydd Porwr Gwe 