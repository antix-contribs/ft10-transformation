��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y       +   4  !   `  E   �  	   �     �     �     �  !   �          9     =  -   B  !   p  &   �  $   �     �     �           	  !     &   <     c     w     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Welsh (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cy
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
 Enw ffeil sain larwm Gofynnwch i ddechrau pob set (pob amserydd) Gofynnwch i ddechrau pob amserydd Cau arddangosfa bar cynnydd yn awtomatig pan ddaw'r holl setiau i ben Cyfluniad Hyd Daw pob amserydd i ben Wedi gorffen. Dangosydd Rhyngwyneb i Sysmonitor Gosodiadau Amserydd Lluosog Enw Byth Nifer o weithiau i redeg y set (pob amserydd) Neges naid pan ddaw pob set i ben Neges naid pan ddaw pob amserydd i ben Diweddariad Bar Cynnydd bob x eiliad Barod i ddechrau ATAL CYFRIFIADUR Eiliadau Eiliadau!Munudau Larwm sain pan ddaw pob set i ben Larwm sain pan ddaw pob amserydd i ben Unedau hyd amserydd Amseryddion wedi dod i ben. 