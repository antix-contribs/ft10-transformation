��          T      �       �      �      �      �   
   �   k        p  �  �     '     =     Y  	   o  z   y     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Welsh (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cy
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN YCHWANEGU Parth amser FT10-cloc Mewnosod Parth Amser i'w ychwanegu at Gloc y Byd \n (Teipiwch ddim i'w chwilio drwy'r holl gylchfaoedd amser sydd ar gael) SYMUD Parth Amser 