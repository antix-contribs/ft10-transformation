��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     6  2   Q  #   �  A   �     �     �     �  	     !   &     H     h     q  /   �  -   �  2   �  *        =     P     e     m  +   �  0   �     �  	   �     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Filipino (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fil/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
 Filename ng tunog ng alarm Hilingin na simulan ang bawat set (lahat ng timer) Hilingin na simulan ang bawat timer Auto close progress bar display kapag natapos na ang lahat ng set Configuration Tagal Ang bawat timer ay nagtatapos Tapos na. Interface sa Sysmonitor Indicator Mga setting ng Maramihang Timer Pangalan Hindi kailanman Bilang ng beses na tatakbo set (lahat ng timer) Pop-up na mensahe kapag natapos ang bawat set Pop-up na mensahe kapag nagtatapos ang bawat timer Pag-update ng Progress Bar bawat x segundo Handa ng magsimula SUSPEND ANG COMPUTER Segundo Segundo!Mga minuto Tunog ang alarm kapag natapos ang bawat set Tunog ang alarm kapag nagtatapos ang bawat timer Mga yunit ng tagal ng timer Mga timer ay natapos na. 