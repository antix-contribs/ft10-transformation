��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  {       �  ,   �  A   �  @        F  $   R  0   w     �     �                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Estonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/et/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: et
Plural-Forms: nplurals=2; plural=(n != 1);
 LISA IKON!add:FBTN Valige tööriistaribale lisamiseks rakendus Topeltklõpsake mis tahes rakendusel selle ikooni teisaldamiseks: Topeltklõpsake mis tahes rakendusel selle ikooni eemaldamiseks: Ikoon asub! LIIGISTAMISEKON!gtk-go-back-rtl:FBTN Ikooni ei leitud, kasutatakse Gearsi vaikeikooni EEMALDA IKON!remove:FBTN Tint2 ikoonid 