��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  t       |  H   �  Z   �  `   ?  '   �  *   �  Z   �  !   N     p                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Japanese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 アイコンを追加!add:FBTN ツールバーに追加するアプリケーションを選択します アプリケーションをダブルクリックして、アイコンを移動します。 アプリケーションをダブルクリックして、そのアイコンを削除します。 アイコンが見つかりました！ アイコンを移動!gtk-go-back-rtl:FBTN デフォルトの歯車アイコンを使用して、アイコンが見つかりません アイコンを削除!remove:FBTN Tint2アイコン 