��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y     �  4     (   D  O   m  
   �     �     �     �     �     	     #     '  1   +  &   ]  ,   �  /   �     �     �            "   -  +   P     |     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Kurdish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ku/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ku
Plural-Forms: nplurals=2; plural=(n != 1);
 Navê pelê dengê alarmê Bipirsin ku her set dest pê bikin (hemû demjimêr) Bipirsin ku her demjimêr dest pê bikin Dema ku hemî set biqede, bara pêşkeftinê ya otomatîkî girtî nîşan dide Veavakirin Demajok Dawiya her demjimêr Qediya. Navbera nîşana Sysmonitor Mîhengên Timer Multiple Nav Qet Hejmara demên ku hatine danîn (hemû demjimêr) Dema ku her set diqede, peyamek pop-up Dema ku her demjimêr diqede, peyamek pop-up Barê Pêşveçûnê her x saniyeyan nûve dike Ji bo destpêkirinê amade ye KOMPYÛRÊ BIQEDÎNIN Seconds Seconds!Minutes Dema ku her set biqede alarma deng Dema ku her demjimêrek biqede alarmek deng yekîneyên duration Timer Timers bi dawî bûye. 