��          T      �       �      �      �      �   
   �   k        p  �  �          5     Q     g     w     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Latvian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN PIEVIENOT laika joslu FT10 pulkstenis Ievietojiet laika joslu, kas jāpievieno Pasaules pulkstenim \n (Ievadiet neko, lai meklētu visās pieejamajās laika joslās) NOŅEMT laika joslu 