��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  !     -   9     g  K   �     �     �     �  	   �  !        '     E     L  3   R  /   �  3   �  4   �          ,     <     E  -   W  ,   �     �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Latvian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
 Trauksmes skaņas faila nosaukums Lūdziet sākt katru komplektu (visi taimeri) Lūdziet sākt katru taimeri Automātiski aizveriet progresa joslas displeju, kad beidzas visi komplekti Konfigurācija Ilgums Katra taimera beigas Pabeigts. Saskarne ar Sysmonitor indikatoru Vairāki taimera iestatījumi Vārds Nekad Iestatīts palaišanas reižu skaits (visi taimeri) Uznirstošais ziņojums, kad katra kopa beidzas Uznirstošais ziņojums, kad beidzas katrs taimeris Progresa josla tiek atjaunināta ik pēc x sekundēm Gatavs sākt APTURĒT DATORU Sekundes Sekundes!Minūtes Skaņas signāls, kad katrs komplekts beidzas Skaņas signāls, kad beidzas katrs taimeris Taimera ilguma vienības Taimeri ir beidzies. 