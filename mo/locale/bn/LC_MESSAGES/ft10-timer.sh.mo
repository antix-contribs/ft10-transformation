��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y  E   �  k   :  O   �  �   �     w     �  2   �     �  ?   �  8   2  	   k     u  W   �  T   �  ]   6	  Z   �	  ?   �	  4   /
     d
  %   z
  i   �
  r   
  8   }     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Bengali (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bn
Plural-Forms: nplurals=2; plural=(n != 1);
 অ্যালার্ম শব্দ ফাইলের নাম প্রতিটি সেট শুরু করতে বলুন (সমস্ত টাইমার) প্রতিটি টাইমার শুরু করতে বলুন সমস্ত সেট শেষ হলে অটো ক্লোজ প্রগ্রেস বার ডিসপ্লে কনফিগারেশন সময়কাল প্রতিটি টাইমার শেষ সমাপ্ত Sysmonitor নির্দেশক ইন্টারফেস একাধিক টাইমার সেটিংস নাম কখনই না সেট চালানোর সংখ্যা (সমস্ত টাইমার) প্রতিটি সেট শেষ হলে পপ-আপ বার্তা প্রতিটি টাইমার শেষ হলে পপ-আপ বার্তা প্রতি x সেকেন্ডে অগ্রগতি বার আপডেট শুরু করার জন্য প্রস্তুত সাসপেন্ড কম্পিউটার সেকেন্ড সেকেন্ড!মিনিট প্রতিটি সেট শেষ হলে অ্যালার্ম শব্দ করুন প্রতিটি টাইমার শেষ হলে অ্যালার্ম শব্দ করুন টাইমার সময়কাল ইউনিট টাইমার শেষ হয়েছে. 