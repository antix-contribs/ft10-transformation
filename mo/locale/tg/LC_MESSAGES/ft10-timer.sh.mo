��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  x  y  +   �  o     0   �  {   �     ;     V  5   i     �  =   �  6   �     .     5  c   C  \   �  ]   	  ;   b	     �	  '   �	     �	     �	  k   
  k   z
  2   �
          *        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Tajik (https://www.transifex.com/antix-linux-community-contributions/teams/120110/tg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tg
Plural-Forms: nplurals=2; plural=(n != 1);
 Номи файли садои ҳушдор Барои оғоз кардани ҳар як маҷмӯа хоҳиш кунед (ҳамаи таймерҳо) Ҳар як таймерро оғоз кунед Намоиши сатри пешрафти худкор ҳангоми ба охир расидани ҳама маҷмӯи Конфигуратсия Давомнокӣ Ҳар як таймер ба охир мерасад Ба охир расид. Интерфейс ба Нишондиҳандаи Sysmonitor Танзимоти таймери чандкарата Ном Ҳеҷ гоҳ Миқдори вақт барои иҷро кардани танзим (ҳама таймерҳо) Паёми поп-ап вақте ки ҳар як маҷмӯи ба охир мерасад Паёми поп-ап ҳангоми ба охир расидани ҳар як таймер Навсозии Бар пешрафт ҳар х сония Омода ба оғоз КОМПЮТЕРРО МЕТАВОНЕД Сонияхо Сонияхо!Дакикахо Ҳангоми ба охир расидани ҳар як маҷмӯи ҳушдор садо медиҳад Ҳангоми ба охир расидани ҳар як таймер ҳушдор садо медиҳад Воҳидҳои давомнокии таймер Таймерҳо ба охир расид. 