��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  x  P     �     �     �     �     �     �     �               "  3   *     ^  	   m     w          �  �   �     '  
   ,     7     R     Y     a     h  $   p     �     �     �     �     �     �     �     �     �     	     	     	     	  
   	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Dutch (https://www.transifex.com/antix-linux-community-contributions/teams/120110/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 Accessoires Allemaal Audio Browser Rekenmachine Klok Controlecentrum Ontwikkeling E-mail Uitgang Favoriete apps: (klik hier om de lijst te bewerken) Bestandsbeheer Bestanden Spellen Grafische afbeeldingen internet Klik met de linkermuisknop - werkbalkpictogrammen toevoegen, verplaatsen en verwijderen; Klik met de rechtermuisknop - Werkbalk beheren Menu Multimedia Netwerkbeheerder - Connman Nieuws Kantoor Foto's recente Zoeken naar programma's en bestanden Instellingen Bureaublad tonen App stoppen Systeem Taakwisselaar Terminal Tekst Teksteditor Koppel USB-drives los Video Volume Weer Weer Webbrowser 