��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  x  y     �  +        7  M   V     �     �     �  	   �  #   �     �            /   !  &   Q  (   x  /   �     �     �     �       %     '   ;     c     v     }        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Dutch (https://www.transifex.com/antix-linux-community-contributions/teams/120110/nl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
Plural-Forms: nplurals=2; plural=(n != 1);
 Bestandsnaam alarmgeluid Vraag om elke set te beginnen (alle timers) Vraag om elke timer te starten Weergave van de voortgangsbalk automatisch sluiten wanneer alle sets eindigen Configuratie Looptijd Elke timer eindigt Afgerond. Interface naar Sysmonitor-indicator Meerdere timerinstellingen Naam Nooit Aantal keren te draaien ingesteld (alle timers) Pop-upbericht wanneer elke set eindigt Pop-upbericht wanneer elke timer afloopt Voortgangsbalk wordt elke x seconden bijgewerkt Klaar om te starten COMPUTER UITSCHAKELEN seconden seconden!Minuten Geluidsalarm wanneer elke set eindigt Geluidsalarm wanneer elke timer eindigt Timerduur eenheden Timers is beëindigd. 