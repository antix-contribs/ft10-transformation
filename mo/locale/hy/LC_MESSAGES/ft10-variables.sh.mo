��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  {  P     �     �     �     �            !   2     T     g     l  m   s  !   �       
             *  �   ;     	     	  '   '	     O	     j	     }	     �	  +   �	     �	  )   �	     
     &
  -   7
     e
  
   v
     �
  #   �
     �
     �
     �
     �
     �
                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Armenian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hy
Plural-Forms: nplurals=2; plural=(n != 1);
 Աքսեսուարներ Բոլորը Աուդիո Բրաուզերը Հաշվիչ Ժամացույց Վերահսկիչ կենտրոն Զարգացում Էլ Ելք Սիրված հավելվածներ. (սեղմեք այստեղ՝ ցանկը խմբագրելու համար) Ֆայլերի կառավարիչ Ֆայլեր Խաղեր Գրաֆիկա Համացանց Ձախ սեղմում - Ավելացնել, տեղափոխել, հեռացնել Գործիքադարակի պատկերակները; Աջ սեղմեք - Կառավարեք Գործիքադարակը Մենյու Մուլտիմեդիա Ցանցի կառավարիչ - Connman Նորություններ Գրասենյակ Նկարներ Վերջերս Որոնեք ծրագրեր և ֆայլեր Կարգավորումներ Ցուցադրել աշխատասեղան Stop հավելվածը Համակարգ Առաջադրանքների փոխարկիչ Տերմինալ Տեքստ Տեքստի խմբագիր Անջատեք USB կրիչները Տեսանյութ Ծավալը Եղանակ Եղանակ Վեբ զննարկիչը 