��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  {  y  C   �  o   9  /   �  �   �     e     �  6   �     �  1   �  8     
   O  
   Z  l   e  v   �  w   I	  q   �	     3
  %   P
     v
  %   �
  o   �
  i   #  8   �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Armenian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hy
Plural-Forms: nplurals=2; plural=(n != 1);
 Զարթուցիչի ձայնային ֆայլի անվանումը Խնդրեք սկսել յուրաքանչյուր հավաքածու (բոլոր ժամանակաչափերը) Խնդրեք սկսել ամեն ժամաչափ Ավտոմատ փակել առաջընթացի գծի ցուցադրումը, երբ բոլոր հավաքածուները ավարտվեն Կոնֆիգուրացիա Տեւողությունը Յուրաքանչյուր ժամաչափի ավարտ Ավարտված: Ինտերֆեյս Sysmonitor-ի ցուցիչին Բազմաթիվ ժմչփի կարգավորումներ Անուն Երբեք Աշխատելու ժամերի քանակը սահմանված է (բոլոր ժամանակաչափերը) Թռուցիկ հաղորդագրություն, երբ յուրաքանչյուր հավաքածուն ավարտվի Թռուցիկ հաղորդագրություն, երբ յուրաքանչյուր ժամաչափ ավարտվում է Առաջընթացի սանդղակը թարմացվում է յուրաքանչյուր x վայրկյանում Պատրաստ է սկսել ԿԱՍԵՑԵԼ ՀԱՄԱԿԱՐԳԻՉԸ Վայրկյաններ Վայրկյաններ!Րոպեներ Ձայնային ազդանշան, երբ յուրաքանչյուր հավաքածուն ավարտվում է Ձայնային ազդանշան, երբ յուրաքանչյուր ժամաչափ ավարտվում է Ժամաչափի տևողության միավորներ Ժամաչափեր ավարտվել է. 