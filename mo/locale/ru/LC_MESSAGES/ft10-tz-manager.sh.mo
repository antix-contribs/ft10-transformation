��          T      �       �      �      �      �   
   �   k        p    �     �     �  (   �     �  �   �  &   �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Russian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ДОБАВИТЬ Часовой пояс FT10-часы Вставьте часовой пояс для добавления в мировые часы \n (Ничего не вводите для поиска по всем доступным часовым поясам) УДАЛИТЬ Часовой пояс 