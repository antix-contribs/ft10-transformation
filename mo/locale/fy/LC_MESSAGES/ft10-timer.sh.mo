��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     �  .     "   A  8   d     �     �     �  	   �  "   �     �            )     #   <  %   `  #   �     �     �     �     �     �      
     +     ?  
   F        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Western Frisian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fy/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fy
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarm lûd triemnamme Freegje om elke set te begjinnen (alle timers) Freegje om elke timer te begjinnen Auto slute fuortgongsbalke werjefte as alle sets einigje Konfiguraasje Doer Eltse timer ein Finished. Ynterface nei Sysmonitor Indicator Meardere timer ynstellings Namme Nea Oantal kearen te rinnen set (alle timers) Pop-up berjocht as elke set einiget Pop-up berjocht as elke timer einiget Progress Bar update elke x sekonden Klear om te begjinnen SUSPEND COMPUTER Sekonden Sekonden!Minuten Lûd alarm as elke set einiget Lûd alarm as elke timer einiget Timer doer ienheden Timers is einige. 