��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  p  P  '   �     �  $   �  !   $  '   F     n  !   �     �     �     �  g   �  '   G     o  	   |     �  $   �  �   �     h	  !   u	  @   �	     �	     �	  	   �	     
  9   
     U
  '   t
     �
     �
     �
     �
     �
  9     +   M     y     �     �     �  -   �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Thai (https://www.transifex.com/antix-linux-community-contributions/teams/120110/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
 เครื่องประดับ ทั้งหมด เครื่องเสียง เบราว์เซอร์ เครื่องคิดเลข นาฬิกา ศูนย์ควบคุม การพัฒนา อีเมล ทางออก แอปโปรด: (คลิกที่นี่เพื่อแก้ไขรายการ) ตัวจัดการไฟล์ ไฟล์ เกม กราฟิก อินเทอร์เน็ต คลิกซ้าย - เพิ่ม, ย้าย, ลบไอคอน Toolbar; คลิกขวา- จัดการแถบเครื่องมือ เมนู มัลติมีเดีย ผู้จัดการเครือข่าย - Connman ข่าว สำนักงาน ภาพ ล่าสุด ค้นหาโปรแกรมและไฟล์ การตั้งค่า แสดงเดสก์ท็อป หยุดแอป ระบบ ตัวสลับงาน เทอร์มินัล ข้อความ โปรแกรมแก้ไขข้อความ ถอดปลั๊กไดรฟ์ USB วีดีโอ ปริมาณ สภาพอากาศ สภาพอากาศ เว็บเบราว์เซอร์ 