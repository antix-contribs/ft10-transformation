��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  =   �  k   1  @   �  �   �     �     �  &   �     �  1   
  ;   <  	   x     �  h   �  {   �  �   t	  Z   �	  )   U
  (   
     �
  &   �
  �   �
  �   �  <        Q  1   g        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Persian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fa/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fa
Plural-Forms: nplurals=2; plural=(n > 1);
 ﺭﺍﺪﺸﻫ ﮓﻧﺯ ﯼﺍﺪﺻ ﻞﯾﺎﻓ ﻡﺎﻧ (ﺎﻫﺮﻤﯾﺎﺗ ﻪﻤﻫ) ﻪﻋﻮﻤﺠﻣ ﺮﻫ ﻉﻭﺮﺷ ﯼﺍﺮﺑ ﺖﺳﺍﻮﺧﺭﺩ ﺪﯿﻫﺍﻮﺨﺑ ﺮﻤﯾﺎﺗ ﺮﻫ ﻉﻭﺮﺷ ﺯﺍ ﺪﺳﺭ ﯽﻣ ﻥﺎﯾﺎﭘ ﻪﺑ ﺎﻫ ﻪﻋﻮﻤﺠﻣ ﻪﻤﻫ ﻪﮐ ﯽﻧﺎﻣﺯ ﺶﯾﺎﻤﻧ ﺖﻓﺮﺸﯿﭘ ﺭﺍﻮﻧ ﺭﺎﮐﺩﻮﺧ ﻥﺪﺷ ﻪﺘﺴﺑ ﯼﺪﻨﺑﺮﮑﯿﭘ ﻥﺎﻣﺯ ﺕﺪﻣ ﺮﻤﯾﺎﺗ ﺮﻫ ﻥﺎﯾﺎﭘ .ﻩﺪﺷ ﻡﺎﻤﺗ ﺮﮕﻧﺎﺸﻧ Sysmonitor ﻪﺑ ﻂﺑﺍﺭ ﻪﻧﺎﮔﺪﻨﭼ ﺮﻤﯾﺎﺗ ﺕﺎﻤﯿﻈﻨﺗ ﻡﺎﻧ ﺰﮔﺮﻫ (ﺎﻫﺮﻤﯾﺎﺗ ﻪﻤﻫ) ﻩﺪﺷ ﻢﯿﻈﻨﺗ ﺍﺮﺟﺍ ﺕﺎﻌﻓﺩ ﺩﺍﺪﻌﺗ ﭖﺁ ﭖﺎﭘ ﻡﺎﯿﭘ ،ﺪﺳﺭ ﯽﻣ ﻥﺎﯾﺎﭘ ﻪﺑ ﻪﻋﻮﻤﺠﻣ ﺮﻫ ﻪﮐ ﯽﻣﺎﮕﻨﻫ ﺩﻮﺷ ﯽﻣ ﺯﺎﺑ ﯽﻣﺎﯿﭘ ،ﺪﺳﺭ ﯽﻣ ﻥﺎﯾﺎﭘ ﻪﺑ ﺮﻤﯾﺎﺗ ﺮﻫ ﻪﮐ ﯽﻣﺎﮕﻨﻫ ﺩﻮﺷ ﯽﻣ ﺯﻭﺭ ﻪﺑ ﻪﯿﻧﺎﺛ x ﺮﻫ ﺖﻓﺮﺸﯿﭘ ﺭﺍﻮﻧ ﻉﻭﺮﺷ ﯼﺍﺮﺑ ﻩﺩﺎﻣﺁ ﺮﺗﻮﯿﭙﻣﺎﮐ ﻖﯿﻠﻌﺗ ﺎﻫ ﻪﯿﻧﺎﺛ ﺎﻫ ﻪﯿﻧﺎﺛ!ﻖﯾﺎﻗﺩ ﺪﯾﺁ ﯽﻣ ﺭﺩ ﺍﺪﺻ ﻪﺑ ﺭﺍﺪﺸﻫ ﮓﻧﺯ ﺪﺳﺭ ﯽﻣ ﻥﺎﯾﺎﭘ ﻪﺑ ﻪﻋﻮﻤﺠﻣ ﺮﻫ ﻪﮐ ﯽﻣﺎﮕﻨﻫ ﺪﯾﺁ ﯽﻣ ﺭﺩ ﺍﺪﺻ ﻪﺑ ﺭﺍﺪﺸﻫ ﮓﻧﺯ ﺩﻮﺷ ﯽﻣ ﻡﺎﻤﺗ ﺮﻤﯾﺎﺗ ﺮﻫ ﻪﮐ ﯽﻧﺎﻣﺯ ﺮﻤﯾﺎﺗ ﻥﺎﻣﺯ ﺕﺪﻣ ﯼﺎﻫﺪﺣﺍﻭ ﺎﻫﺮﻤﯾﺎﺗ .ﺖﺳﺍ ﻩﺪﯿﺳﺭ ﻥﺎﯾﺎﭘ ﻪﺑ 