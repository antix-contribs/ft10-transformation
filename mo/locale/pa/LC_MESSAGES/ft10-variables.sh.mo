��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P     �     �               %  	   A  "   K     n     ~     �  �   �     #     C     V     f       �   �     f	     s	  =   �	     �	     �	     �	     
  V   
     n
  %   �
     �
     �
     �
  "   �
          .  @   N     �     �     �     �  "   �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Panjabi (Punjabi) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pa/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pa
Plural-Forms: nplurals=2; plural=(n != 1);
 ਸਹਾਇਕ ਉਪਕਰਣ ਸਾਰੇ ਆਡੀਓ ਬਰਾਊਜ਼ਰ ਕੈਲਕੁਲੇਟਰ ਘੜੀ ਕੰਟਰੋਲ ਕੇਂਦਰ ਵਿਕਾਸ ਈ - ਮੇਲ ਨਿਕਾਸ ਮਨਪਸੰਦ ਐਪਸ: (ਸੂਚੀ ਨੂੰ ਸੰਪਾਦਿਤ ਕਰਨ ਲਈ ਇੱਥੇ ਕਲਿੱਕ ਕਰੋ) ਫਾਈਲ ਮੈਨੇਜਰ ਫਾਈਲਾਂ ਖੇਡਾਂ ਗ੍ਰਾਫਿਕਸ ਇੰਟਰਨੈੱਟ ਖੱਬਾ ਕਲਿਕ- ਜੋੜੋ, ਮੂਵ ਕਰੋ, ਟੂਲਬਾਰ ਆਈਕਨ ਹਟਾਓ; ਸੱਜਾ ਕਲਿੱਕ ਕਰੋ- ਟੂਲਬਾਰ ਦਾ ਪ੍ਰਬੰਧਨ ਕਰੋ ਮੀਨੂ ਮਲਟੀਮੀਡੀਆ ਨੈੱਟਵਰਕ ਮੈਨੇਜਰ - ਕੌਨਮੈਨ ਖ਼ਬਰਾਂ ਦਫ਼ਤਰ ਤਸਵੀਰਾਂ ਹਾਲ ਹੀ ਪ੍ਰੋਗਰਾਮਾਂ ਅਤੇ ਫਾਈਲਾਂ ਦੀ ਖੋਜ ਕਰੋ ਸੈਟਿੰਗਾਂ ਡੈਸਕਟਾਪ ਦਿਖਾਓ ਐਪ ਨੂੰ ਰੋਕੋ ਸਿਸਟਮ ਟਾਸਕ ਸਵਿੱਚਰ ਅਖੀਰੀ ਸਟੇਸ਼ਨ ਟੈਕਸਟ ਟੈਕਸਟ ਐਡੀਟਰ USB ਡਰਾਈਵਾਂ ਨੂੰ ਅਨਪਲੱਗ ਕਰੋ ਵੀਡੀਓ ਵਾਲੀਅਮ ਮੌਸਮ ਮੌਸਮ ਵੈੱਬ ਬਰਾਊਜ਼ਰ 