��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  |  y     �  *        7  >   U     �     �     �     �  !   �     �       	     2     )   D  .   n  )   �     �     �     �     �  $     %   (     N     ^     e        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Afrikaans (https://www.transifex.com/antix-linux-community-contributions/teams/120110/af/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: af
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarm klank lêernaam Vra om elke stel te begin (alle aftellers) Vra om elke afteller te begin Outo toemaak vorderingsbalk vertoon wanneer alle stelle eindig Konfigurasie Duur Elke tydhouer eindig Klaar. Koppelvlak na Sysmonitor-aanwyser Veelvuldige timer-instellings Naam Nooit nie Aantal kere om te hardloop gestel (alle aftellers) Opspringboodskap wanneer elke stel eindig Opspringboodskap wanneer elke tydteller eindig Vorderingsbalk-opdatering elke x sekondes Gereed om te begin SAK REKENAAR OP Sekondes Sekondes!Minute Klink alarm wanneer elke stel eindig Klink alarm wanneer elke timer eindig Tydsduureenhede Timers geëindig het. 