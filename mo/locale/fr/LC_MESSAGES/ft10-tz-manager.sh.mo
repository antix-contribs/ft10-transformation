��          T      �       �      �      �      �   
   �   k        p  �  �                ?     V  �   c     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Wallon Wallon, 2022
Language-Team: French (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 $add_tz_text!ajouter : FBTN $delete_tz_text!supprimer : FBTN Ajouter fuseau horaire FT10-horloge Insérez le fuseau horaire à ajouter à l'horloge mondiale \n (n'entrez rien pour chercher tous les fuseaux horaires disponibles). Supprimer fuseau horaire 