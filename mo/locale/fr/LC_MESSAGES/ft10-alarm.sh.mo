��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  �  �  (   1  '   Z  B   �  '   �     �  G         H     W     ^     r     �     �  	   �  
   �     �  	   �  C   �  L        Z     a     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Wallon Wallon, 2022
Language-Team: French (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
     \t<b>$alarm_set_to_text $h $min</b>      \t<b>$alarm_text $h $min...</b> \n    Êtes-vous sûr de mettre le réveil à cette heure-là demain ? $alarm_set_to_text $h $min
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarme Alarme réglée sur Êtes-vous sûr ??? Annuler l'alarme Durée Heure::CB Intervalle Minutes::CB Répéter Pour annuler, cliquez avec le bouton droit de la souris sur le menu Pour annuler, cliquez avec le bouton droit de la souris sur l'icône du menu infini secondes 