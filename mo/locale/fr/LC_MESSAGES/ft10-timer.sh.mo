��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  &   �  8   !  &   Z  i   �     �     �             &   !  #   H     l     p  @   w  3   �  9   �  =   &     d     w     �     �  *   �  +   �  !   �  
   	     $	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Wallon Wallon, 2022
Language-Team: French (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 Nom du fichier pour le son de l'alarme Proposer de commencer l'ensemble (toutes les minuteries) Proposer de commencer chaque minuterie Fermeture automatique de l'affichage de la barre de progression lorsque tous les ensembles sont terminés Configuration Durée Chaque fin de minuterie Terminé Interface avec l'indicateur Sysmonitor Réglages multiples de la minuterie Nom Jamais Nombre de fois pour exécuter l'ensemble (toutes les minuteries) Message dans un pop-up à la fin de chaque ensemble Message dans un pop-up lorsque chaque minuterie s'arrête Mise à jour de la barre de progression toutes les x secondes Prêt à démarrer MISE EN VEILLE Secondes Secondes!Minutes Alarme sonore à la fin de chaque ensemble Alarme sonore à la fin de chaque minuterie Unités de durée de la minuterie Minuteries a pris fin. 