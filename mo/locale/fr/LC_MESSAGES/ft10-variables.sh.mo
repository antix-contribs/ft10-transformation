��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  �  P     �     �     �  
   �     �                     +     4  7   <     t     �     �  	   �     �  s   �     "     '  !   3     U     a     h     o  '   w     �     �     �     �     �     �     	     	  #   	     >	     E	     L	     T	     \	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Wallon Wallon, 2022
Language-Team: French (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 Accessoires Tout Audio Navigateur Calculatrice Horloge Centre de contrôle Développement Courriel Quitter App préférées : (cliquez ici pour modifier la liste) Gestionnaire de fichiers Fichiers Jeux Graphisme Internet Clic gauche : ajouter, déplacer, supprimer des icônes de la barre d'outils. Clic droit : gérer la barre d'outils Menu Multimédia Connman - Gestionnaire de réseau Actualités Bureau Images Récent Chercher des programmes et des fichiers Paramètres Afficher le bureau Forcer l'arrêt d'une app Système Commutateur de tâches Terminal Texte Éditeur de textes Débrancher les périphériques USB Vidéo Volume Météo Météo Navigateur Web 