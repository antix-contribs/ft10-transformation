��          T      �       �      �      �      �   
   �   k        p  �  �     B     X     t     �  �   �                                             $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Gaelic, Scottish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/gd/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gd
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Sòn ùine FT10-uaireadair Cuir a-steach Sòn Ùine ri chur ri Cloc na Cruinne \n (Na cuir a-steach dad airson sgrùdadh tro na sònaichean ùine a tha rim faighinn) CUIR Sòn ùine 