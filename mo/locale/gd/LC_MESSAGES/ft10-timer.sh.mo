��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     :  6   V      �  [   �     
               /  $   ?     d     {     �  =   �  7   �  7   �  "   6     Y     u     �     �  7   �  7   �     	  	   #	     -	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Gaelic, Scottish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/gd/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gd
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
 Ainm faidhle fuaim rabhaidh Iarr air tòiseachadh air gach seata (a h-uile timers) Iarr air tòiseachadh gach timer Dùin taisbeanadh bàr adhartais gu fèin-ghluasadach nuair a thig a h-uile seata gu crìch Rèiteachadh Fad Deireadh gach timer Crìochnaichte. Eadar-aghaidh gu Comharra Sysmonitor Roghainnean ioma-Timer Ainm A-riamh An àireamh de thursan airson seata a ruith (a h-uile timers) Teachdaireachd pop-up nuair a thig gach seata gu crìch Teachdaireachd pop-up nuair a thig gach timer gu crìch Ùrachadh bar adhartas gach x diog Deiseil airson tòiseachadh SUSPEND COMPUTER Diogan Diogan!Mionaidean Inneal-rabhaidh fuaim nuair a thig gach seata gu crìch Inneal-rabhaidh fuaim nuair a thig gach timer gu crìch Aonadan ùine timer Luchd-ama air tighinn gu crìch. 