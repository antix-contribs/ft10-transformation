��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y     �  9     &   L  J   s     �     �     �     �  '   �  +        F     J  9   N  )   �  1   �  4   �          .     D     K  %   Y  +     #   �     �  
   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Catalan (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Nom del fitxer de so d'alarma Demana que comenci cada conjunt (tots els temporitzadors) Demaneu que comenci cada temporitzador Tancament automàtic de la barra de progrés quan acabin tots els conjunts Configuració Durada Cada cronòmetre acaba Acabat. Interfície a l'indicador de Sysmonitor Múltiples configuracions del temporitzador Nom Mai Nombre de vegades que s'executa (tots els temporitzadors) Missatge emergent quan acabi cada conjunt Missatge emergent quan s'acabi cada temporitzador Actualització de la barra de progrés cada x segons A punt per començar SUSPENDRE L'ORDINADOR segons segons!Minuts Sona l'alarma quan acabi cada conjunt Sona l'alarma quan acabi cada temporitzador Unitats de durada del temporitzador Temporitzadors ha acabat. 