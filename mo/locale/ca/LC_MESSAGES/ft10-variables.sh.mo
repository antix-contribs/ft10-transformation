��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  z  P  
   �     �     �  	   �     �     �               #     6  =   >     |     �     �     �     �  f   �               $  	   >     H     P     V     ]     w     �     �     �     �     �     �     �     �     	     	     	     	     $	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Catalan (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Accessoris Tots Àudio Navegador Calculadora Rellotge Centre de control Desenvolupament Correu electrònic Sortida Aplicacions preferides: (feu clic aquí per editar la llista) Gestor d'arxius Fitxers Jocs Gràfics Internet Clic esquerre: Afegeix, mou, elimina icones de la barra d'eines; Clic dret - Gestiona la barra d'eines Menú Multimèdia Gestor de xarxa - Connman Notícies Oficina Fotos Recent Cerca programes i fitxers Configuració Mostra l'escriptori Atura l'aplicació Sistema Canviador de tasques Terminal Text Editor de textos Desconnecteu les unitats USB Vídeo Volum El temps El temps Navegador web 