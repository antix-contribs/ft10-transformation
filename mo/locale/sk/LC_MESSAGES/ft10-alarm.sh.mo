��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  �  �  (   u  '   �  ;   �  '        *  G   =     �     �     �     �     �     �  
   �     �     �     �  ?   �  ?   ?          �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Slovak (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Si si istý, že si zajtra nastavíš budík na ten čas? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Budík nastavený na Si si istý??? Zrušiť budík Trvanie hodina::CB Interval Minúty::CB Opakujte Ak chcete zrušiť, kliknite pravým tlačidlom myši na ponuku Pre zrušenie kliknite pravým tlačidlom myši na ikonu ponuky nekonečné sekúnd 