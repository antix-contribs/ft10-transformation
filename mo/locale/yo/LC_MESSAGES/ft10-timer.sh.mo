��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  r  y     �  8     #   @  R   d  
   �  	   �     �     �     �     	           &  5   *  7   `  5   �  5   �               ,     1  +   @  ,   l     �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Yoruba (https://www.transifex.com/antix-linux-community-contributions/teams/120110/yo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: yo
Plural-Forms: nplurals=1; plural=0;
 Orukọ faili ohun itaniji Beere lati bẹrẹ eto kọọkan (gbogbo awọn akoko) Beere lati bẹrẹ aago kọọkan Ifihan ọpa ilọsiwaju sunmọ ni aifọwọyi nigbati gbogbo awọn eto ba pari Iṣeto ni Iye akoko Ipari aago kọọkan Ti pari. Ni wiwo to Sysmonitor Atọka Awọn eto Aago pupọ Oruko Kò Nọmba awọn akoko lati ṣeto (gbogbo awọn aago) Ifiranṣẹ agbejade nigbati ṣeto kọọkan ba pari Ifiranṣẹ agbejade nigbati aago kọọkan ba pari Ilọsiwaju Pẹpẹ imudojuiwọn gbogbo x iṣẹju Ṣetan lati bẹrẹ KỌMPUTA dáduro Aaya Aaya!Iṣẹju Itaniji ohun nigbati eto kọọkan ba pari Itaniji ohun nigbati aago kọọkan ba pari Aago iye sipo Aago ti pari. 