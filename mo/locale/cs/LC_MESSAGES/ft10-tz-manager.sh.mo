��          T      �       �      �      �      �   
   �   k        p  �  �     J     `     |     �  �   �     2                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Czech (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN PŘIDAT časové pásmo Hodiny FT10 Vložte časové pásmo, které chcete přidat do světového času \n (Chcete-li prohledat všechna dostupná časová pásma, nezadávejte nic) ODSTRANIT Časové pásmo 