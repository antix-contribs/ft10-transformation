��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  <   �  C   $  D   h     �  %   �  D   �     ,     H                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Czech (https://www.transifex.com/antix-linux-community-contributions/teams/120110/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 PŘIDAT IKONU!add:FBTN Vyberte aplikaci, kterou chcete přidat na lištu nástrojů Dvojitým kliknutím na kteroukoli aplikaci přesunete její ikonu: Dvojitým kliknutím na kteroukoli aplikaci odstraníte její ikonu: Ikona se nachází! PŘESUNOUT IKONU!gtk-go-back-rtl:FBTN Není umístěna žádná ikona, používá se výchozí ikona Gears ODSTRANIT IKONU!remove:FBTN Tint2 ikony 