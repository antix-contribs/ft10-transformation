��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  y  P  	   �     �     �     �     �     �     �  	               /        J     Y     _     d     k  b   w     �     �     �                         "     A     O     ^     g     n     |     �     �     �     �     �     �     �  
   �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Danish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 tilbehør Alle Lyd Browser Lommeregner Ur Kontrolcenter Udvikling E-mail Afslut Yndlingsapps: (klik her for at redigere listen) Filhåndtering Filer Spil Grafik Internettet Venstre klik- Tilføj, flyt, fjern værktøjslinjeikoner; Højreklik - Administrer værktøjslinje Menu Multimedier Netværksleder - Connman Nyheder Kontor Billeder Nylig Søg efter programmer og filer Indstillinger Vis skrivebord Stop app System Opgaveskifter Terminal Tekst Teksteditor Frakobl USB-drev Video Bind Vejr Vejr Webbrowser 