��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y     �  *        0  @   L     �     �     �     �  "   �     �     �       -     &   6  &   ]  %   �     �     �     �     �  "   �  "        0     G     N        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Danish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarm lyd filnavn Bed om at begynde hvert sæt (alle timere) Bed om at starte hver timer Automatisk lukning af statuslinjevisning, når alle sæt slutter Konfiguration Varighed Hver timer slutter Færdig. Interface til Sysmonitor-indikator Flere timerindstillinger Navn Aldrig Antal gange at køre indstillet (alle timere) Pop op-besked, når hvert sæt slutter Pop op-besked, når hver timer slutter Statuslinje-opdatering hvert x sekund Klar til at starte SUSPEND COMPUTEREN Sekunder Sekunder!Referat Lyd alarm, når hvert sæt slutter Lyd alarm, når hver timer slutter Timer varighed enheder Timere er afsluttet. 