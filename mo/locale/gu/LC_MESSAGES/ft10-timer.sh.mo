��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  {  y  E   �  `   ;  J   �  �   �     �     �  &   �     �  Q     ;   `  	   �     �  Q   �  z   	  �   �	  k   

  :   v
  7   �
     �
  "   �
  �     �   �  )   9     c  .   s        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Gujarati (https://www.transifex.com/antix-linux-community-contributions/teams/120110/gu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gu
Plural-Forms: nplurals=2; plural=(n != 1);
 એલાર્મ સાઉન્ડ ફાઇલનું નામ દરેક સેટ શરૂ કરવા માટે કહો (બધા ટાઈમર) દરેક ટાઈમર શરૂ કરવા માટે કહો જ્યારે બધા સેટ સમાપ્ત થાય ત્યારે પ્રોગ્રેસ બાર ડિસ્પ્લેને સ્વતઃ બંધ કરો રૂપરેખાંકન અવધિ દરેક ટાઈમર અંત સમાપ્ત. સિસ્મોનિટર સૂચક માટે ઇન્ટરફેસ બહુવિધ ટાઈમર સેટિંગ્સ નામ ક્યારેય સેટ ચલાવવાની સંખ્યા (બધા ટાઈમર) જ્યારે દરેક સેટ સમાપ્ત થાય ત્યારે પોપ-અપ સંદેશ જ્યારે દરેક ટાઈમર સમાપ્ત થાય ત્યારે પોપ-અપ સંદેશ દર x સેકન્ડમાં પ્રોગ્રેસ બાર અપડેટ થાય છે શરૂ કરવા માટે તૈયાર છે સસ્પેન્ડ કોમ્પ્યુટર સેકન્ડ સેકન્ડ!મિનિટ જ્યારે દરેક સેટ સમાપ્ત થાય ત્યારે એલાર્મ સાઉન્ડ કરો જ્યારે દરેક ટાઈમર સમાપ્ત થાય ત્યારે એલાર્મ સાઉન્ડ કરો ટાઈમર અવધિ એકમો ટાઈમર સમાપ્ત થઈ ગયું છે. 