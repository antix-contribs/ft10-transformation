��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  y  y  B   �  �   6  ^   �  �   !     �     �  /   �     (  ?   B  C   �     �     �  l   �  b   V	  e   �	  t   
  C   �
  P   �
     )  7   H  g   �  v   �  2   _     �  "   �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Kannada (https://www.transifex.com/antix-linux-community-contributions/teams/120110/kn/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: kn
Plural-Forms: nplurals=2; plural=(n > 1);
 ಅಲಾರ್ಮ್ ಸೌಂಡ್ ಫೈಲ್ ಹೆಸರು ಪ್ರತಿ ಸೆಟ್ ಅನ್ನು ಪ್ರಾರಂಭಿಸಲು ಕೇಳಿ (ಎಲ್ಲಾ ಟೈಮರ್u200cಗಳು) ಪ್ರತಿ ಟೈಮರ್ ಅನ್ನು ಪ್ರಾರಂಭಿಸಲು ಕೇಳಿ ಎಲ್ಲಾ ಸೆಟ್u200cಗಳು ಕೊನೆಗೊಂಡಾಗ ಸ್ವಯಂ ಕ್ಲೋಸ್ ಪ್ರೋಗ್ರೆಸ್ ಬಾರ್ ಡಿಸ್u200cಪ್ಲೇ ಸಂರಚನೆ ಅವಧಿ ಪ್ರತಿ ಟೈಮರ್ ಅಂತ್ಯ ಮುಗಿದಿದೆ. Sysmonitor ಸೂಚಕಕ್ಕೆ ಇಂಟರ್ಫೇಸ್ ಬಹು ಟೈಮರ್ ಸೆಟ್ಟಿಂಗ್u200cಗಳು ಹೆಸರು ಎಂದಿಗೂ ರನ್ ಮಾಡಲು ಸಮಯಗಳ ಸಂಖ್ಯೆ (ಎಲ್ಲಾ ಟೈಮರ್u200cಗಳು) ಪ್ರತಿ ಸೆಟ್ ಕೊನೆಗೊಂಡಾಗ ಪಾಪ್-ಅಪ್ ಸಂದೇಶ ಪ್ರತಿ ಟೈಮರ್ ಕೊನೆಗೊಂಡಾಗ ಪಾಪ್-ಅಪ್ ಸಂದೇಶ ಪ್ರತಿ x ಸೆಕೆಂಡಿಗೆ ಪ್ರೋಗ್ರೆಸ್ ಬಾರ್ ಅಪ್u200cಡೇಟ್ ಪ್ರಾರಂಭಿಸಲು ಸಿದ್ಧವಾಗಿದೆ ಕಂಪ್ಯೂಟರ್ ಅನ್ನು ಅಮಾನತುಗೊಳಿಸಿ ಸೆಕೆಂಡುಗಳು ಸೆಕೆಂಡುಗಳು!ನಿಮಿಷಗಳು ಪ್ರತಿ ಸೆಟ್ ಕೊನೆಗೊಂಡಾಗ ಎಚ್ಚರಿಕೆಯ ಧ್ವನಿ ಪ್ರತಿ ಟೈಮರ್ ಕೊನೆಗೊಂಡಾಗ ಅಲಾರಾಂ ಧ್ವನಿಸುತ್ತದೆ ಟೈಮರ್ ಅವಧಿಯ ಘಟಕಗಳು ಟೈಮರ್u200cಗಳು ಕೊನೆಗೊಂಡಿದೆ. 