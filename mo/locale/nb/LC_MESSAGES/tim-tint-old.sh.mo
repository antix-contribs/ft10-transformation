��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  3   �  1   �  1        =  !   Q  -   s     �     �                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Norwegian Bokmål (https://www.transifex.com/antix-linux-community-contributions/teams/120110/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 LEGG TIL IKONET!add:FBTN Velg programmet du vil legge til på verktøylinjen Dobbeltklikk på et program for å flytte ikonet: Dobbeltklikk på et program for å fjerne ikonet: Ikonet er plassert! FLYTT IKONET!gtk-go-back-rtl:FBTN Ingen ikon funnet, bruker standard Gears-ikon FJERN IKONET!remove:FBTN Tint2-ikoner 