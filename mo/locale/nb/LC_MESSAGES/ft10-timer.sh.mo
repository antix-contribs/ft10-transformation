��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y     �  +        <  =   Z     �     �     �     �  $   �     �            -     '   D  (   l  *   �     �     �     �     �  "     $   (     M  	   d     n        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Norwegian Bokmål (https://www.transifex.com/antix-linux-community-contributions/teams/120110/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarm lyd filnavn Be om å starte hvert sett (alle tidtakere) Be om å starte hver tidtaker Automatisk lukk fremdriftslinjevisning når alle sett slutter Konfigurasjon Varighet Hver tidtaker slutter Ferdig. Grensesnitt til Sysmonitor-indikator Flere timerinnstillinger Navn Aldri Antall ganger å kjøre satt (alle tidtakere) Popup-melding når hvert sett avsluttes Popup-melding når hver tidtaker slutter Fremdriftslinje oppdatering hvert x sekund Klar til å starte SUSPENDER DATAMASKINEN Sekunder Sekunder!Minutter Lydalarm når hvert sett avsluttes Lyd alarm når hver tidtaker slutter Timer varighet enheter Tidtakere har sluttet. 