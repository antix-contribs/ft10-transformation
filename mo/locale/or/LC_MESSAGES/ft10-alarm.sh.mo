��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  w  �  (   (  '   Q  �   y  '        D  G   W     �     �  1   �  )   �  7        U     b     x     �  6   �  {   �  x   X     �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Odia (https://www.transifex.com/antix-linux-community-contributions/teams/120110/or/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: or
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    ଆସନ୍ତାକାଲି ସେହି ସମୟ ପାଇଁ ଆପଣ ଆଲାର୍ମ ସେଟ୍ କରିବାକୁ ନିଶ୍ଚିତ କି? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title ଆଲାର୍ମ ଆଲାର୍ମ ସେଟ୍ ହୋଇଛି | ତମେ ନିଶ୍ଚିତ କି??? ଆଲାର୍ମ ବାତିଲ କରନ୍ତୁ | ଅବଧି ଘଣ୍ଟା |::CB ବ୍ୟବଧାନ ମିନିଟ୍::CB ପୁନରାବୃତ୍ତି କରନ୍ତୁ | ବାତିଲ୍ କରିବାକୁ, ମେନୁ ପାଇଁ ଡାହାଣ କ୍ଲିକ୍ କରନ୍ତୁ | ବାତିଲ୍ କରିବାକୁ, ମେନୁ ପାଇଁ ଡାହାଣ କ୍ଲିକ୍ ଆଇକନ୍ | ଅସୀମ | ସେକେଣ୍ଡ୍ | 