��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  |  y  E   �  \   <  >   �  �   �     g     �  '   �     �  9   �  9        I     P  [   ]  Z   �  \   	  a   q	  $   �	     �	     
     !
  P   =
  R   �
  F   �
     (     7        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Bulgarian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 Име на файла със звуков звук за аларма Помолете да започнете всеки набор (всички таймери) Искайте да започнете всеки таймер Автоматично затваряне на лентата за напредък, когато всички набори приключат Конфигурация Продължителност Всеки край на таймера Готово. Интерфейс към индикатора Sysmonitor Множество настройки на таймера име никога Брой пъти за стартиране е зададен (всички таймери) Изскачащо съобщение, когато всеки набор приключи Изскачащо съобщение, когато всеки таймер приключи Лентата за напредък се актуализира на всеки x секунди Готови да започнете СПРИ КОМПЮТЪР Секунди Секунди!Минути Звукова аларма, когато всеки набор приключи Звукова аларма, когато всеки таймер приключи Единици за продължителност на таймера Таймери свърши. 