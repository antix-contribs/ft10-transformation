��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  |  P     �     �  
   �     �               -     M     ^     |  m   �     �          &     /     >    O     n	     w	  '   �	     �	     �	     �	     �	  6   �	     
  1   2
  %   d
     �
  ,   �
     �
  
   �
     �
  /     
   2     =  '   V  '   ~     �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Bulgarian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 Аксесоари всичко аудио Браузър Калкулатор Часовник Контролен център Развитие Електронна поща Излезте Любими приложения: (щракнете тук, за да редактирате списъка) Файлов мениджър Файлове игри графики интернет Щракнете с левия бутон - Добавяне, преместване, премахване на иконите на лентата с инструменти; Щракнете с десния бутон - Управление на лентата с инструменти Меню Мултимедия Мрежов мениджър - Connman Новини офис Снимки Последни Търсене на програми и файлове Настройки Показване на работния плот Спрете приложението Система Превключвател на задачи терминал Текст Текстов редактор Изключете USB устройствата Видео Сила на звука Метеорологично време Метеорологично време Уеб браузър 