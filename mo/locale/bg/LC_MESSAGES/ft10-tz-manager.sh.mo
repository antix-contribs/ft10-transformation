��          T      �       �      �      �      �   
   �   k        p  |  �     �       &   0     W  �   m  3   \                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Bulgarian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ДОБАВЕТЕ часова зона FT10-часовник Въведете часова зона, която да бъде добавена към Световния часовник \n (Въведете нищо, за да търсите във всички налични часови зони) ПРЕМАХВАНЕ на часовата зона 