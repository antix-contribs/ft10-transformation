��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  |  y     �  *        6  >   T     �     �     �     �      �     �     �       2     $   ;  *   `  /   �     �     �     �     �  %   �  +   !     M     c  	   p        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Esperanto (https://www.transifex.com/antix-linux-community-contributions/teams/120110/eo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1);
 Alarma sondosiernomo Petu komenci ĉiun aron (ĉiuj tempigiloj) Petu komenci ĉiun tempigilon Aŭtomate fermita progresbreto ekrano kiam ĉiuj aroj finiĝas Agordo Daŭro Ĉiu temporizilo finiĝas Finita. Interfaco al Sysmonitor Indikilo Multoblaj Timer-agordoj Nomo Neniam Nombro da fojoj por funkcii aro (ĉiuj tempigiloj) Ŝprucmesaĝo kiam ĉiu aro finiĝas Ŝprucmesaĝo kiam ĉiu tempigilo finiĝas Ĝisdatigo de Progreso-Treno ĉiujn x sekundojn Preta por komenci SUSPENDI KOMPUTILON Sekundoj Sekundoj!Minutoj Sonigu alarmon kiam ĉiu aro finiĝas Sonigu alarmon kiam ĉiu tempigilo finiĝas Tempigilo daŭrounuoj Temporiziloj finiĝis. 