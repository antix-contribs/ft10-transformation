��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  |  P     �     �     �     �  	   �     �            	        "  <   (     e     {     �     �  	   �  ^   �     �  	             #     ,     4  
   :     E     e     m  
   �     �     �  	   �     �     �     �     �     �     �     �     �                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Esperanto (https://www.transifex.com/antix-linux-community-contributions/teams/120110/eo/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1);
 Akcesoraĵoj Ĉiuj Aŭdio Retumilo Kalkulilo Horloĝo Kontrolcentro Evoluo Retpoŝto Eliro Plej ŝatataj Aplikoj: (klaku ĉi tie por redakti la liston) Dosiera Administranto Dosieroj Ludoj Grafikoj Interreto Maldekstre klako- Aldoni, movi, forigi ilobreto-ikonoj; Dekstre alklaku - Administri Ilobreton Menuo Plurmedia Retmanaĝero - Connman Novaĵoj Oficejo Fotoj Lastatempa Serĉu programojn kaj dosierojn Agordoj Montru Labortablon Haltu Apon Sistemo Taskoŝanĝilo Terminalo Teksto Tekstredaktilo Malkonekti USB-diskojn Video Volumo Vetero Vetero Retumilo 