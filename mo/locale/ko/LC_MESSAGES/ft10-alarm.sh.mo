��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  r  �  (   #  '   L  =   t  '   �     �  G   �     5     D     K     Y     l     z     �     �     �     �  N   �  X   �     M     T     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Korean (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ko/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ko
Plural-Forms: nplurals=1; plural=0;
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    내일 그 시간으로 알람을 설정하시겠습니까? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title 경보 알람 설정 확실합니까??? 알람 취소 지속 시::CB 간격 분::CB 반복하다 취소하려면 메뉴를 마우스 오른쪽 버튼으로 클릭하십시오. 취소하려면 메뉴 아이콘을 마우스 오른쪽 버튼으로 클릭하십시오. 무한 초 