��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  |  y     �  >     )   N  L   x     �  
   �     �     �  $        (     F     K  5   P  ,   �  1   �  6   �          .     N     [  6   o  4   �  /   �     	     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Hungarian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Riasztási hangfájlnév Kérje meg az egyes sorozatok indítását (minden időzítő) Kérje meg minden időzítő indítását Automatikus bezárás folyamatjelző sáv, amikor minden készlet véget ér Konfiguráció Időtartam Minden időzítő vége Befejezett. Interfész a Sysmonitor Indicatorhoz Több időzítő beállítás Név Soha A futtatások száma beállítva (összes időzítő) Felugró üzenet az egyes készletek végén Felugró üzenet, amikor minden időzítő lejár A folyamatjelző sáv frissítése x másodpercenként Kezdésre készen SZÁMÍTÓGÉP FELFÜGGESZTÉSE Másodpercek Másodpercek!Percek Hangjelzés hangzik, amikor minden készlet véget ér Hangjelzés hangzik, amikor minden időzítő lejár Az időzítő időtartamának mértékegységei Időzítők befejeződött. 