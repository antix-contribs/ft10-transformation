��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y  %   �  1        L  X   k     �  	   �     �  
   �  %     *   *     U     \  E   b  /   �  3   �  6        C     V     j     s  )   �  0   �  &   �     	     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Spanish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Nombre de archivo de sonido de alarma Pida comenzar cada serie (todos los cronómetros) Pida comenzar cada cronómetro Pantalla de barra de progreso de cierre automático cuando finalizan todos los conjuntos Configuración Duración Cada final del temporizador Terminado. Interfaz para el indicador Sysmonitor Múltiples configuraciones de temporizador Nombre Nunca Número de veces para ejecutar el conjunto (todos los temporizadores) Mensaje emergente cuando finaliza cada conjunto Mensaje emergente cuando finaliza cada temporizador Actualización de la barra de progreso cada x segundos Listo para empezar SUSPENDER ORDENADOR Segundos Segundos!Minutos Suena la alarma cuando termina cada juego Suena la alarma cuando termina cada temporizador Unidades de duración del temporizador Temporizadores ha terminado. 