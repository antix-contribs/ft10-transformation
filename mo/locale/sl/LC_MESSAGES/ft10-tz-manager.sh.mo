��          T      �       �      �      �      �   
   �   k        p  �  �     2     H     d     w     �                                              $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Slovenian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN DODAJ časovni pas FT10-ura Vstavite časovni pas, ki ga želite dodati svetovni uri \n (Vnesite nič za iskanje po vseh razpoložljivih časovnih pasovih) ODSTRANI Časovni pas 