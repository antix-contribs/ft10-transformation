��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  y  P     �     �     �     �     �     �     �     	            ;   #     _     o     w     ~     �  o   �     �  
             .     :     @     G     O     h     v     �     �     �     �     �  
   �     �     �     �     �     	  
   	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Zubehör Alle Audio Browser Taschenrechner Uhr Kontrollzentrum Entwicklung Email Beenden Lieblingsprogramme: (Zum Bearbeiten der Liste hier klicken) Dateiverwaltung Dateien Spiele Grafik Internet Linksklick – Symbolleistensymbole hinzufügen, verschieben, entfernen; Rechtsklick – Symbolleiste verwalten Menü Multimedia Netzwerkverwaltung — Connman Nachrichten Büro Bilder Aktuell Programm- und Dateisuche Einstellungen Arbeitsfläche anzeigen Programm anhalten System Aufgabenumschalter Terminal Text Texteditor USB-Laufwerke ausstecken Video Lautstärke Wetter Wetter Webbrowser 