��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  y       �  L   �  X   �  V   C     �  '   �  I   �          :                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 SYMBOL HINZUFÜGEN!add:FBTN Wählen Sie die Anwendung aus, die der Symbolleiste hinzugefügt werden soll Mit einem Doppelklick auf eine beliebige Anwendung können Sie deren Symbol verschieben: Mit einem Doppelklick auf eine beliebige Anwendung können Sie deren Symbol entfernen: Symbol gefunden! SYMBOL VERSCHIEBEN!gtk-go-back-rtl:FBTN Kein Symbol gefunden, es wird das standardmäßige Gears-Symbol verwendet SYMBOL ENTFERNEN!remove:FBTN Tint2-Symbole 