��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y  6   �     +  Y   �  �        �     �  =   �     3  C   D  2   �     �  "   �  q   �  p   W	  p   �	  y   9
  8   �
  7   �
     $  (   4  }   ]  �   �  &   c     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Sinhala (https://www.transifex.com/antix-linux-community-contributions/teams/120110/si/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: si
Plural-Forms: nplurals=2; plural=(n != 1);
 එලාම් ශබ්ද ගොනු නාමය එක් එක් කට්ටලය ආරම්භ කිරීමට අසන්න (සියලු කාල ගණක) එක් එක් ටයිමරය ආරම්භ කිරීමට අසන්න සියලු කට්ටල අවසන් වූ විට ස්වයංක්u200dරීයව ප්u200dරගති තීරු සංදර්ශකය වසා දමන්න මානකරනය කාල සීමාව එක් එක් ටයිමරය අවසන් වේ අවසන්. Sysmonitor දර්ශකයට අතුරු මුහුණත බහු ටයිමර් සැකසුම් නම කවදාවත් නැහැ ධාවනය කළ යුතු වාර ගණන සකසා ඇත (සියලු කාල ගණක) එක් එක් කට්ටලය අවසන් වන විට උත්පතන පණිවිඩය එක් එක් ටයිමරය අවසන් වන විට උත්පතන පණිවිඩය ප්u200dරගති තීරුව සෑම තත්පරයකම යාවත්කාලීන කරන්න ආරම්භ කිරීමට සූදානම් පරිගණකය අත්හිටුවන්න තත්පර තත්පර!මිනිත්තු එක් එක් කට්ටලය අවසන් වන විට අනතුරු ඇඟවීමේ ශබ්දය එක් එක් කාල ගණකයක් අවසන් වන විට අනතුරු ඇඟවීමේ ශබ්දය ටයිමර් කාල ඒකක ටයිමර් අවසාන වුණා. 