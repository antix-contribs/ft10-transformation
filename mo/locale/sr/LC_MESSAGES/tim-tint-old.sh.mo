��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �  �       �  u   �  t   a     �     V  &   t  l   �  #        ,                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Serbian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 ДОДАЈ ИКОНУ!add:FBTN Изаберите апликацију коју желите да додате на траку са алаткама Двапут кликните на било коју апликацију да померите њену икону: Двапут кликните на било коју апликацију да бисте уклонили њену икону: Икона се налази! МОВЕ ИЦОН!gtk-go-back-rtl:FBTN Није пронађена икона, користи се подразумевана икона Геарс УКЛОНИ ИКОНУ!remove:FBTN Тинт2 иконе 