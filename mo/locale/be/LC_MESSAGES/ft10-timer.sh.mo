��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n    y  3   �  I   �  3   �  z   3     �     �  ,   �       7     *   U     �     �  N   �  j   �  l   N	  Q   �	     
      '
     H
     W
  a   u
  c   �
  4   ;     p             	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Belarusian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/be/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: be
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Імя файла гуку сігналізацыі Прасіць пачаць кожны набор (усе таймеры) Прасіце пачаць кожны таймер Аўтаматычна закрывае дысплей прагрэсу, калі ўсе наборы сканчаюцца Канфігурацыя Працягласць Кожны таймер сканчаецца Скончаны. Інтэрфейс да індыкатара Sysmonitor Некалькі налад таймера Імя Ніколі Колькасць запускаў зададзена (усе таймеры) Усплывальнае паведамленне, калі кожны набор заканчваецца Усплывальнае паведамленне, калі кожны таймер заканчваецца Панэль прагрэсу абнаўляецца кожныя х секунд Гатовы пачаць ПРЫСЫНІЦЬ КАМП'ЮТ Сэкунды Сэкунды!Хвіліны Гучыць сігнал трывогі, калі заканчваецца кожны набор Гучыць сігнал трывогі, калі кожны таймер заканчваецца Адзінкі працягласці таймера Таймеры скончылася. 