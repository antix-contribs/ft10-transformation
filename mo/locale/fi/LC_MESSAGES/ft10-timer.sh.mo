��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y     �  4     #   F  Q   j  
   �     �     �     �  $   �          +  
   0  0   ;  -   l  /   �  -   �     �               (  ,   >  .   k     �  	   �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Finnish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Hälytysäänen tiedostonimi Pyydä aloittamaan jokainen sarja (kaikki ajastimet) Pyydä aloittamaan jokainen ajastin Automaattinen sulkeminen edistymispalkin näyttö, kun kaikki sarjat päättyvät Kokoonpano Kesto Jokainen ajastimen loppu Valmis. Liitäntä Sysmonitor-indikaattoriin Useita ajastinasetuksia Nimi Ei koskaan Ajokertojen määrä asetettu (kaikki ajastimet) Ponnahdusikkuna, kun jokainen sarja päättyy Ponnahdusviesti, kun jokainen ajastin päättyy Edistymispalkki päivittyy x sekunnin välein Valmiina aloittamaan KESKEÄ TIETOKONE Sekuntia Sekuntia!Pöytäkirja Hälytysääni, kun jokainen sarja päättyy Hälytysääni, kun jokainen ajastin päättyy Ajastimen keston yksiköt Ajastimet on päättynyt. 