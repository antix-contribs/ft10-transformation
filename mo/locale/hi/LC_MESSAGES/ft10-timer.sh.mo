��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  x  y  <   �  v   /  `   �  �        �     �  2   �     �  G     ;   Y  	   �     �  O   �  j   	  p   q	  e   �	  :   H
  B   �
     �
     �
  l   �
  r   `  2   �       -           	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Hindi (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
 अलार्म ध्वनि फ़ाइल नाम प्रत्येक सेट (सभी टाइमर) शुरू करने के लिए कहें प्रत्येक टाइमर शुरू करने के लिए कहें सभी सेट समाप्त होने पर ऑटो क्लोज प्रोग्रेस बार डिस्प्ले विन्यास अवधि प्रत्येक टाइमर अंत ख़त्म होना। Sysmonitor संकेतक के लिए इंटरफ़ेस एकाधिक टाइमर सेटिंग्स नाम कभी नहीँ सेट चलाने की संख्या (सभी टाइमर) प्रत्येक सेट समाप्त होने पर पॉप-अप संदेश प्रत्येक टाइमर समाप्त होने पर पॉप-अप संदेश प्रोग्रेस बार हर x सेकंड में अपडेट करें शुरू करने के लिए तैयार कंप्यूटर को सस्पेंड करें सेकंड सेकंड!मिनट प्रत्येक सेट समाप्त होने पर ध्वनि अलार्म प्रत्येक टाइमर समाप्त होने पर ध्वनि अलार्म टाइमर अवधि इकाइयाँ टाइमर समाप्त हो गया है। 