��          �      l      �  (   �  '   
  7   2  '   j     �  G   �     �     �                    ,     5     >     G     S     Z  $   z     �     �  x  �  (   )  '   R  �   z  '        ;  G   N     �     �     �  0   �  ,        3     @     Q     m     ~  s   �  �        �     �     	                                                                                         
        \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    Are you sure to set the alarm for that time tomorrow? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title Alarm Alarm set to Are you sure??? Cancel Alarm Duration Hour::CB Interval Minutes::CB Repeat To cancel, right-click for menu To cancel, right-click icon for menu infinite seconds Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Hindi (https://www.transifex.com/antix-linux-community-contributions/teams/120110/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
     \t<b>$alarm_set_to_text $hr:$mn</b>      \t<b>$alarm_text $hr:$mn...</b> \n    क्या आप कल उस समय के लिए अलार्म सेट करना सुनिश्चित करेंगे? $alarm_set_to_text $hr:$mn
$cancel_text $are_you_sure_text $cancel_alarm_text !bash -c 'cancel_quit_tray $XPID $target'!gtk-cancel $confirm_title अलार्म अलार्म सेट क्या आपको यकीन है??? अलार्म रद्द करें अवधि घंटा::CB मध्यान्तर मिनट::CB दोहराना रद्द करने के लिए, मेनू के लिए राइट-क्लिक करें रद्द करने के लिए, मेनू के लिए आइकन पर राइट-क्लिक करें अनंत सेकंड 