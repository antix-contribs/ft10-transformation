��          T      �       �      �      �      �   
   �   k        p     �     �     �     �     �  �   �     l                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Lithuanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN PRIDĖTI laiko juostą FT10 laikrodis Įterpkite laiko juostą, kurią norite įtraukti į pasaulio laikrodį \n (Norėdami ieškoti visose galimose laiko juostose, nieko neįveskite) PAŠALINTI laiko juostą 