��    
      l      �       �      �   (     .   ,  0   [     �     �  )   �     �     �            C   &  M   j  O   �            J   7  "   �     �                         	                
    ADD ICON!add:FBTN Choose application to add to the Toolbar Double click any Application to move its icon: Double click any Application to remove its icon: Icon located! MOVE ICON!gtk-go-back-rtl:FBTN No icon located, using default Gears icon REMOVE ICON!remove:FBTN Tint2 icons Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:39+0000
Last-Translator: Robin, 2022
Language-Team: Lithuanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
 PRIDĖTI Piktogramą!add:FBTN Pasirinkite programą, kurią norite įtraukti į įrankių juostą Dukart spustelėkite bet kurią programą, kad perkeltumėte jos piktogramą: Dukart spustelėkite bet kurią programą, kad pašalintumėte jos piktogramą: Piktograma yra! MOVE ICON!gtk-go-back-rtl:FBTN Nerasta jokios piktogramos, naudojant numatytąją „Gears“ piktogramą PAŠALINTI Piktogramą!remove:FBTN Tint2 piktogramos 