��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  x  y  7   �  [   *  F   �  �   �     \     q  .   �     �  3   �  @   �  
   @     K  v   T  M   �  [   	  [   u	      �	  %   �	     
  #   1
  M   U
  _   �
  <        @     ]        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Greek (https://www.transifex.com/antix-linux-community-contributions/teams/120110/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Όνομα αρχείου ήχου συναγερμού Ζητήστε να ξεκινήσετε κάθε σετ (όλα τα χρονόμετρα) Ζητήστε να ξεκινήσετε κάθε χρονόμετρο Αυτόματο κλείσιμο της γραμμής προόδου εμφανίζεται όταν τελειώνουν όλα τα σετ Διαμόρφωση Διάρκεια Τέλος κάθε χρονοδιακόπτη Πεπερασμένος. Διασύνδεση με ένδειξη Sysmonitor Πολλαπλές ρυθμίσεις χρονοδιακόπτη Ονομα Ποτέ Ο αριθμός των φορών για εκτέλεση έχει οριστεί (όλα τα χρονόμετρα) Αναδυόμενο μήνυμα όταν τελειώνει κάθε σετ Αναδυόμενο μήνυμα όταν τελειώνει κάθε χρονόμετρο Η γραμμή προόδου ενημερώνεται κάθε x δευτερόλεπτα Ετοιμος να αρχίσω ΑΝΑΣΤΟΛΗ ΥΠΟΛΟΓΙΣΤΗ Δευτερόλεπτα Δευτερόλεπτα!Λεπτά Ηχητικό ξυπνητήρι όταν τελειώνει κάθε σετ Ηχητικός συναγερμός όταν τελειώνει κάθε χρονόμετρο Μονάδες διάρκειας χρονοδιακόπτη Χρονοδιακόπτες έχει τελειώσει. 