��          T      �       �      �      �      �   
   �   k        p  �  �     E     [     w     �  �   �                                             $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Maltese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mt
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==0 || ( n%100>1 && n%100<11) ? 1 : (n%100>10 && n%100<20 ) ? 2 : 3);
 $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Żona tal-ħin FT10-arloġġ Daħħal iż-Żona tal-Ħin biex tiżdied mal-Arloġġ Dinji \n (Daħħal xejn biex tfittex fiż-żoni tal-ħin kollha disponibbli) NEĦĦI Żona tal-ħin 