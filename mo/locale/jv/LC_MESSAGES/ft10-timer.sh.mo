��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  t  y     �  $        +  ?   K     �     �     �     �  %   �     �     �       -     %   9  '   _  #   �     �     �     �     �  #   �  %   �     $     6     <        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Javanese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/jv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: jv
Plural-Forms: nplurals=1; plural=0;
 Jeneng file swara weker Takon miwiti saben set (kabeh timer) Takon kanggo miwiti saben timer Nutup otomatis tampilan garis kemajuan nalika kabeh set rampung Konfigurasi Duration Saben wektu pungkasan Rampung. Antarmuka kanggo Sysmonitor Indikator Setelan Multiple Timer jeneng ora tau Jumlah kaping kanggo mbukak set (kabeh timer) Pesen pop-up nalika saben set rampung Pesen pop-up nalika saben timer rampung Nganyari Progress Bar saben x detik Siap miwiti TANGGUH KOMPUTER detik detik!menit Weker muni nalika saben set rampung Weker muni nalika saben timer rampung Unit durasi wektu Timer wis rampung. 