��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  z  y     �  )        0  F   N     �     �     �  	   �  %   �     �            /     &   H  *   o  +   �     �     �     �     �        "        B     \     c        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Swedish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Larmljuds filnamn Be att få börja varje set (alla timers) Be att få börja varje timer Stäng förloppsindikatorn automatiskt när alla uppsättningar slutar Konfiguration Varaktighet Varje timer slutar Färdiga. Gränssnitt till Sysmonitor-indikator Flera timerinställningar namn Aldrig Antal gånger att köra inställd (alla timers) Popup-meddelande när varje set slutar Popup-meddelande när varje timer tar slut Förloppsindikatorn uppdateras var x sekund Redo att börja AVSTÄNG DATORN Sekunder Sekunder!Minuter Ljudlarm när varje set slutar Ljudlarm när varje timer tar slut Timer varaktighet enheter Timers har upphört. 