��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y        ;   ?  )   {  I   �     �     �       	   "  %   ,     R     q     v  <   y  *   �  1   �  5        I     \     s     {  )   �  1   �  $   �  
   	     	        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Romanian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ro/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 Nume fișier de sunet de alarmă Solicitați să începeți fiecare set (toate cronometrele) Cereți să începeți fiecare cronometru Închiderea automată a barei de progres când se termină toate seturile Configurare Durată Fiecare cronometru se termină Terminat. Interfață cu indicatorul Sysmonitor Setări multiple de cronometru Nume Nu Numărul de ori pentru rulare setat (toate temporizatoarele) Mesaj pop-up când fiecare set se termină Mesaj pop-up când fiecare cronometru se termină Bara de progres se actualizează la fiecare x secunde Gata să înceapă SUSPENDĂ CALCULATORUL secunde secunde!Minute Sună alarma la sfârșitul fiecărui set Sună alarma când fiecare cronometru se termină Unități de durată a cronometrului Cronometre s-a terminat. 