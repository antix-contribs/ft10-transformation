��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  �  y  :     [   P  G   �  �   �     y     �  )   �     �  :   �  ?   "     b     i  b   x  V   �  \   2	  Y   �	  (   �	  )   
     <
     K
  D   g
  I   �
  @   �
     7     F        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Macedonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/mk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mk
Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;
 Име на датотека за звук за аларм Побарајте да го започнете секој сет (сите тајмери) Побарајте да започнете со секој тајмер Автоматско затворање на лентата за напредок кога ќе завршат сите сетови Конфигурација Времетраење Секој крај на тајмерот Завршено. Индикатор за интерфејс за Sysmonitor Повеќекратни поставки на тајмерот Име Никогаш Поставен е бројот на пати за извршување (сите тајмери) Порака што се појавува кога ќе заврши секој сет Порака што се појавува кога ќе заврши секој тајмер Лентата за напредок се ажурира на секои x секунди Подготвени за почеток СУСПЕНДИРАЈ КОМПЈУТЕР Секунди Секунди!Минути Звучен аларм кога ќе заврши секој сет Звучен аларм кога завршува секој тајмер Единици за времетраење на тајмерот Тајмери заврши. 