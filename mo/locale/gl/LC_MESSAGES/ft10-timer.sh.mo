��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  {  y  !   �  9     "   Q  F   t     �  	   �     �     �  %   �          <     A  <   G  ,   �  0   �  3   �          )     =     F  )   W  -   �  %   �     �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Galician (https://www.transifex.com/antix-linux-community-contributions/teams/120110/gl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl
Plural-Forms: nplurals=2; plural=(n != 1);
 Nome do ficheiro de son de alarma Solicitar comezar cada conxunto (todos os temporizadores) Solicite comezar cada temporizador Peche automaticamente a barra de progreso cando rematan todos os xogos Configuración Duración Cada temporizador remata Rematou. Interface para o indicador Sysmonitor Varias opcións de temporizador Nome Nunca Número de veces que se executará (todos os temporizadores) Mensaxe emerxente cando remate cada conxunto Mensaxe emerxente cando remata cada temporizador Actualización da barra de progreso cada x segundos Listo para comezar SUSPENDIR ORDENADOR Segundos Segundos!Minutos Sonar a alarma cando remate cada conxunto Sonar a alarma cando remate cada temporizador Unidades de duración do temporizador Temporizadores rematou. 