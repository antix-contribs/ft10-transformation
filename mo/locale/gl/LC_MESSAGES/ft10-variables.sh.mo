��    (      \  5   �      p     q     }     �     �  
   �     �     �     �     �     �  ,   �     �                      F        f  
   k     v     �     �     �     �     �     �     �     �     �     �     �                    &     ,     3     ;     D  {  P  
   �     �     �  	   �     �     �                "     6  =   <     z     �     �  	   �     �  �   �     D  
   J     U     n     t     |     �     �     �     �     �     �     �     �     	     	     	     3	     :	     A	     G	     M	                       
          	   $                                                     &   (       !          %          "                             #                    '        Accessories All Audio Browser Calculator Clock Control Centre Development E-mail Exit Favorite Apps: (click here to edit the list) File Manager Files Games Graphics Internet Left click- Add,move,remove Toolbar icons; Right click- Manage Toolbar Menu Multimedia Network manager - Connman News Office Pics Recent Search for programs and files Settings Show Desktop Stop App System Task switcher Terminal Text Text Editor Unplug USB drives Video Volume Weather Weather  Web Browser Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Galician (https://www.transifex.com/antix-linux-community-contributions/teams/120110/gl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl
Plural-Forms: nplurals=2; plural=(n != 1);
 Accesorios Todos Audio Navegador Calculadora Reloxo Centro de control Desenvolvemento Correo electrónico Saír Aplicacións favoritas: (faga clic aquí para editar a lista) Xestor de ficheiros Arquivos Xogos Gráficos Internet Fai clic co botón esquerdo: Engadir, mover, eliminar iconas da barra de ferramentas; Fai clic co botón dereito - Xestionar a barra de ferramentas Menú Multimedia Xestor de rede - Connman Novas Oficina Fotos Recente Busca programas e ficheiros Configuración Mostrar escritorio Parar a aplicación Sistema Cambiador de tarefas Terminal Texto Editor de texto Desconecta as unidades USB Vídeo Volume Tempo Tempo Navegador web 