��          �   %   �      P     Q  "   f     �  1   �     �     �     �  	   �  !        %     =     B  '   H  !   p  #   �  #   �     �     �     �                1     R     g  
   n  q  y     �  9     #   <  P   `     �     �     �     �  !   �          /     3  (   ?  0   h  3   �  0   �     �       	   4     >  '   R  &   z     �  	   �     �        	                                                                             
                                        Alarm sound filename Ask to begin each set (all timers) Ask to begin each timer Auto close progress bar display when all sets end Configuration Duration Each timer end Finished. Interface to Sysmonitor Indicator Multiple Timer settings Name Never Number of times to run set (all timers) Pop-up message when each set ends Pop-up message when each timer ends Progress Bar update every x seconds Ready to start SUSPEND COMPUTER Seconds Seconds!Minutes Sound alarm when each set ends Sound alarm when each timer ends Timer duration units Timers has ended. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: Robin, 2022
Language-Team: Uzbek (https://www.transifex.com/antix-linux-community-contributions/teams/120110/uz/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uz
Plural-Forms: nplurals=1; plural=0;
 Signal ovozi fayl nomi Har bir toʻplamni boshlashni soʻrang (barcha taymerlar) Har bir taymerni boshlashni so'rang Barcha to'plamlar tugagandan so'ng avtomatik yopilish jarayoni paneli ko'rinishi Konfiguratsiya Davomiyligi Har bir taymer tugaydi Tugallandi. Sysmonitor indikatoriga interfeys Bir nechta taymer sozlamalari Ism Hech qachon Ishlash vaqtlari soni (barcha taymerlar) Har bir to'plam tugaganda qalqib chiquvchi xabar Har bir taymer tugashi bilan qalqib chiquvchi xabar Har bir x soniyada taraqqiyot paneli yangilanadi Boshlash uchun tayyor KOMPYUTERNI toʻxtatib qoʻyish soniyalar soniyalar!Daqiqalar Har bir to'plam tugaganda ovozli signal Har bir taymer tugaganda ovozli signal Taymer davomiyligi birliklari Taymerlar tugadi. 