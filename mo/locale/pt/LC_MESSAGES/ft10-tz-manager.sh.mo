��          T      �       �      �      �      �   
   �   k        p  �  �          6     S     k  �   y     �                                        $add_tz_text!add:FBTN $delete_tz_text!remove:FBTN ADD Time zone FT10-clock Insert Time Zone to be added to the World Clock \n (Enter nothing to search trough all available timezones) REMOVE Time zone Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-01-18 00:38+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2022
Language-Team: Portuguese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
 $add_tz_text!adicionar:FBTN $delete_tz_text!remover:FBTN ADICIONAR Zona Horária FT10-relógio Inserir a Zona Horária a adicionar ao Relógio Mundial \n (Deixar em branco para buscar em todas as zonas horárias disponíveis) REMOVER Zona Horária 