
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#: tint2-top-bottom.sh:12
msgid "Toolbar position"
msgstr ""

#: tint2-top-bottom.sh:13
msgid "Select Toolbar position"
msgstr ""

#: tint2-top-bottom.sh:14 tint2-top-bottom.sh:37
msgid "top"
msgstr ""

#: tint2-top-bottom.sh:15 tint2-top-bottom.sh:34
msgid "bottom"
msgstr ""

#: tint2-top-bottom.sh:16 tint2-top-bottom.sh:40
msgid "left"
msgstr ""

#: tint2-top-bottom.sh:17 tint2-top-bottom.sh:43
msgid "right"
msgstr ""

#: tint2-top-bottom.sh:25
msgid "$window_title"
msgstr ""

