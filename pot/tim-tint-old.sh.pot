
# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 01:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#: tim-tint-old.sh:93
msgid "No icon located, using default Gears icon"
msgstr ""

#: tim-tint-old.sh:95
msgid "Icon located!"
msgstr ""

#: tim-tint-old.sh:156
msgid "Tint2 icons"
msgstr ""

#: tim-tint-old.sh:156
msgid "Double click any Application to remove its icon:"
msgstr ""

#: tim-tint-old.sh:216
msgid "Double click any Application to move its icon:"
msgstr ""

#: tim-tint-old.sh:305
msgid "Choose application to add to the Toolbar"
msgstr ""

#: tim-tint-old.sh:327
msgid "ADD ICON!add:FBTN"
msgstr ""

#: tim-tint-old.sh:327
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr ""

#: tim-tint-old.sh:327
msgid "REMOVE ICON!remove:FBTN"
msgstr ""

